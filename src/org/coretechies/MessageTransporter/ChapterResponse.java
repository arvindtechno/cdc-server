/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class ChapterResponse extends MessageResponse{
    
    private ArrayList<String> chapterList;

    public ArrayList<String> getChapterList() {
        return chapterList;
    }

    public void setChapterList(ArrayList<String> chapterList) {
        this.chapterList = chapterList;
    }    
}
