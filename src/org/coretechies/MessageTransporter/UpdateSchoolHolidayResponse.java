/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

/**
 *
 * @author Tiwari
 */
public class UpdateSchoolHolidayResponse extends MessageResponse{

    private boolean response;
    private boolean Interrupt;
    private String subject;
    private String lesson;
    private String topic;
    private String chapter;

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public boolean isInterrupt() {
        return Interrupt;
    }

    public void setInterrupt(boolean Interrupt) {
        this.Interrupt = Interrupt;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }
    
}
