/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class CreateNewLessonRequest extends AddNewLessonTopicDataRequest{

    private ArrayList<CreateLesson> topicList;

    public ArrayList<CreateLesson> getTopicList() {
        return topicList;
    }

    public void setTopicList(ArrayList<CreateLesson> topicList) {
        this.topicList = topicList;
    }

}
