/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Tiwari
 */
public class AddNewLessonChapterDataResponse extends MessageResponse{

    private ArrayList<String> chapterList;
    private ArrayList<Integer> classDayScheduleList;
    private ArrayList<Date> dateList;

    public ArrayList<String> getChapterList() {
        return chapterList;
    }

    public void setChapterList(ArrayList<String> chapterList) {
        this.chapterList = chapterList;
    }

    public ArrayList<Integer> getClassDayScheduleList() {
        return classDayScheduleList;
    }

    public void setClassDayScheduleList(ArrayList<Integer> classDayScheduleList) {
        this.classDayScheduleList = classDayScheduleList;
    }

    public ArrayList<Date> getDateList() {
        return dateList;
    }

    public void setDateList(ArrayList<Date> dateList) {
        this.dateList = dateList;
    }
    
}
