/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class ClassDayScheduleResponse extends MessageResponse {
    
    private ArrayList<Integer> scheduleList;

    public ArrayList<Integer> getScheduleList() {
        return scheduleList;
    }

    public void setScheduleList(ArrayList<Integer> scheduleList) {
        this.scheduleList = scheduleList;
    }
}
