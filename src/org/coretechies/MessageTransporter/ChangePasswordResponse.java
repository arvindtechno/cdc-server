/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

/**
 *
 * @author Tiwari
 */
public class ChangePasswordResponse extends MessageResponse{
    
    private int changePasswordResponse;

    public int getChangePasswordResponse() {
        return changePasswordResponse;
    }

    public void setChangePasswordResponse(int changePasswordResponse) {
        this.changePasswordResponse = changePasswordResponse;
    }
    
}
