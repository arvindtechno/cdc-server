/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Tiwari
 */
public class AddNewLessonTopicDataResponse extends MessageResponse{

    private ArrayList<AddNewLesson> topicList;
    private ArrayList<Date> leaveDates;

    public ArrayList<AddNewLesson> getTopicList() {
        return topicList;
    }

    public void setTopicList(ArrayList<AddNewLesson> topicList) {
        this.topicList = topicList;
    }

    public ArrayList<Date> getLeaveDates() {
        return leaveDates;
    }

    public void setLeaveDates(ArrayList<Date> leaveDates) {
        this.leaveDates = leaveDates;
    }

}
