/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.MessageTransporter;

import java.util.ArrayList;

/**
 *
 * @author Tiwari
 */
public class UpdateUserResponse extends MessageResponse{

    private ArrayList<UpdateUser> updateUsers;
    private String access_type;

    public ArrayList<UpdateUser> getUpdateUsers() {
        return updateUsers;
    }

    public void setUpdateUsers(ArrayList<UpdateUser> updateUsers) {
        this.updateUsers = updateUsers;
    }

    public String getAccess_type() {
        return access_type;
    }

    public void setAccess_type(String access_type) {
        this.access_type = access_type;
    }
}
