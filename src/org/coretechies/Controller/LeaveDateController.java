/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.LeaveDateResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class LeaveDateController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        LeaveDateResponse leaveDateResponse = new LeaveDateResponse();
        
        leaveDateResponse.setMessageType("LeaveDateResponse");
        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "(select userName from session where IPAddress = \'"+IP+"\')";
            ResultSet rs = stmt.executeQuery(query);        
            if(rs.next()){
                query = "SELECT holiday FROM cdc_beta.school_holiday where school_district_Id = (select school_district_Id from school_user where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\')))";
//                query = "SELECT holiday FROM cdc_beta.school_holiday where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\'))";
                System.out.println("Query : "+query);
                rs = stmt.executeQuery(query);
                ArrayList<Date> leaveDateList = new ArrayList<>();
                while(rs.next()){
                    leaveDateList.add(rs.getDate("holiday"));
                }
                leaveDateResponse.setLeaveDateList(leaveDateList);
                
                query = "SELECT classdate FROM cdc_beta.lesson_plan where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\'))";
                System.out.println("Query : "+query);
                rs = stmt.executeQuery(query);
                ArrayList<Date> classDateList = new ArrayList<>();
                while(rs.next()){
                    classDateList.add(rs.getDate("classdate"));
                }
                leaveDateResponse.setClassDateList(classDateList);
                
            }
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                leaveDateResponse.setHash(rs.getString("SHA"));
            
            return leaveDateResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
