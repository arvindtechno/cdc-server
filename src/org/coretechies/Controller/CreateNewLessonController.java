/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.CreateNewLessonRequest;
import org.coretechies.MessageTransporter.CreateNewLessonResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class CreateNewLessonController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        CreateNewLessonRequest createNewLessonRequest = (CreateNewLessonRequest) messageRequest;
        CreateNewLessonResponse createNewLessonResponse = new CreateNewLessonResponse();

        createNewLessonResponse.setMessageType("CreateNewLessonResponse");
        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "select * from school_user inner join user_info using(user_id) where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\'))";
            System.out.println("Query : "+ query);
            ResultSet rs = stmt.executeQuery(query);
            ArrayList<String> dates = new ArrayList<>();
            if(rs.next()){
                String userId = rs.getString("user_id");
                String userName = rs.getString("username");
                String district = rs.getString("school_district_Id");
                
                int size = createNewLessonRequest.getTopicList().size();
                for(int i=0; i<size; i++){
                    query = "INSERT INTO `lesson_plan` (`user_id`, `school_district_Id`, `subject`, `chapter`, `lesson`, `topic`, `grade`, `curriculum`, `classdate`, `created_by`, `updated_by`) VALUES"
                        + " (\'"+userId+"\', \'"+district+"\', \'"+createNewLessonRequest.getSubject()+"\', \'"+createNewLessonRequest.getChapter()+"\', \'"+createNewLessonRequest.getLesson()+"\', \'"+createNewLessonRequest.getTopicList().get(i).getTopic()+"\', \'"+createNewLessonRequest.getGrade()+"\', \'"+createNewLessonRequest.getCurriculum()+"\', \'"+createNewLessonRequest.getTopicList().get(i).getDate()+"\', \'"+userName+"\', \'"+userName+"\')";
                    System.out.println("Query : "+query);
                    System.out.println("Query : "+createNewLessonRequest.getTopicList().get(i).getTopic());
                    int ii = stmt.executeUpdate(query);
                    if(ii==1){
                        dates.add(createNewLessonRequest.getTopicList().get(i).getDate());
                    }
                }
            }
            createNewLessonResponse.setDateList(dates);
            System.out.println("Dates"+dates.toString());
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                createNewLessonResponse.setHash(rs.getString("SHA"));
            
            return createNewLessonResponse;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return createNewLessonResponse;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
