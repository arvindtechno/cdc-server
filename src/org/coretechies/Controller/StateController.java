/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.LessonResponse;
import org.coretechies.MessageTransporter.LessonRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.StateRequest;
import org.coretechies.MessageTransporter.StateResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class StateController {

    public MessageResponse getData(MessageRequest messageRequest, String IP){
     
        StateRequest stateRequest = (StateRequest) messageRequest;
        StateResponse stateResponse = new StateResponse();
        stateResponse.setMessageType("StateResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT state FROM state where country_id = (select country_id from country where country= \'"+stateRequest.getCountry()+"\'";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            ArrayList<String> stateList = new ArrayList<>();
            while(rs.next())
                stateList.add(rs.getString("state"));
            
            stateResponse.setStateList(stateList);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                stateResponse.setHash(rs.getString("SHA"));
            
            return stateResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int checkValidation(MessageRequest messageRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(messageRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }
}
