/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.AddNewLesson;
import org.coretechies.MessageTransporter.AddNewLessonTopicDataRequest;
import org.coretechies.MessageTransporter.AddNewLessonTopicDataResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class AddNewLessonTopicController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        AddNewLessonTopicDataRequest addNewLessonTopicDataRequest = (AddNewLessonTopicDataRequest) messageRequest;
        AddNewLessonTopicDataResponse addNewLessonTopicDataResponse = new AddNewLessonTopicDataResponse();
        
        addNewLessonTopicDataResponse.setMessageType("AddNewLessonTopicDataResponse");

        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            
            Calendar cal = Calendar.getInstance();
            String query = "SELECT * FROM topic_grade left join topic on topic.topic_id = topic_grade.Topic_Id where topic not in (SELECT topic FROM cdc_beta.lesson_plan where curriculum = \'"+addNewLessonTopicDataRequest.getCurriculum()+"\' and grade = \'"+addNewLessonTopicDataRequest.getGrade()+"\' and subject = \'"+addNewLessonTopicDataRequest.getSubject()+"\' and chapter = \'"+addNewLessonTopicDataRequest.getChapter()+"\' and lesson = \'"+addNewLessonTopicDataRequest.getLesson()+"\' and classdate >= \'"+cal.get(Calendar.YEAR)+"/"+(cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.DATE)+"\')";
//          String query = "SELECT * FROM topic_grade left join topic on topic.topic_id = topic_grade.Topic_Id where curriculum = \'"+addNewLessonTopicDataRequest.getCurriculum()+"\' and grade = \'"+addNewLessonTopicDataRequest.getGrade()+"\' and subject = \'"+addNewLessonTopicDataRequest.getSubject()+"\' and chapter = \'"+addNewLessonTopicDataRequest.getChapter()+"\' and lesson = \'"+addNewLessonTopicDataRequest.getLesson()+"\'";
            System.out.println("Query : "+ query);
            ResultSet rs = stmt.executeQuery(query);

            ArrayList<AddNewLesson> arrayList = new ArrayList<>();
            while(rs.next()){
                AddNewLesson addNewLesson = new AddNewLesson();
                addNewLesson.setId(rs.getString("topic_id"));
                addNewLesson.setTopic(rs.getString("topic"));
                arrayList.add(addNewLesson);
            }
            addNewLessonTopicDataResponse.setTopicList(arrayList);
    
            query = "SELECT holiday FROM cdc_beta.school_holiday where school_district_Id = (select school_district_Id from school_user where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\')))";
            System.out.println("Query : "+query);
            rs = stmt.executeQuery(query);
            ArrayList<Date> leaveDateList = new ArrayList<>();
            while(rs.next()){
                leaveDateList.add(rs.getDate("holiday"));
            }            
            query = "SELECT holiday FROM cdc_beta.user_leave where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\'))";
            System.out.println("Query : "+query);
            rs = stmt.executeQuery(query);
            while(rs.next()){
                leaveDateList.add(rs.getDate("holiday"));
            }
            addNewLessonTopicDataResponse.setLeaveDates(leaveDateList);
            
        
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                addNewLessonTopicDataResponse.setHash(rs.getString("SHA"));
            
            return addNewLessonTopicDataResponse;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
