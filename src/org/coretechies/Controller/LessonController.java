/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.LessonResponse;
import org.coretechies.MessageTransporter.LessonRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class LessonController {

    private SingleTon singleTon;
    
    public MessageResponse getData(MessageRequest messageRequest, String IP){
     
        LessonRequest lessonRequest = (LessonRequest) messageRequest;
        LessonResponse lessonResponse = new LessonResponse();
        lessonResponse.setMessageType("LessonDataResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT topic FROM topic_grade left join topic on topic.topic_id = topic_grade.Topic_Id where curriculum = \'"+singleTon.getCurriculum()+"\' and grade = \'"+singleTon.getGrade()+"\' and subject = \'"+singleTon.getSubject()+"\' and chapter = \'"+singleTon.getChapter()+"\' and lesson = \'"+lessonRequest.getChapterName()+"\'";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            ArrayList<String> arrayList = new ArrayList<>();
            while(rs.next())
                arrayList.add(rs.getString("topic"));
            
            lessonResponse.setLessonList(arrayList);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                lessonResponse.setHash(rs.getString("SHA"));
            
            return lessonResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    
    public int checkValidation(LessonRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }
    
    public void setSingleTonData(MessageRequest messageRequest) {
        singleTon = SingleTon.getSingleTon();
        LessonRequest lessonRequest = (LessonRequest) messageRequest;
        singleTon.setLesson(lessonRequest.getChapterName());
    }
}
