/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

/**
 *
 * @author Tiwari
 */
public class SingleTon {

    static SingleTon singleTon;
    
    private String curriculum;
    private String grade;
    private String subject;
    private String chapter;
    private String lesson;
    private String topic;
    
    static{
        singleTon = null;
    }
    
    private SingleTon() {
    }
    
    public static SingleTon getSingleTon(){
        if(singleTon == null){
            singleTon = new SingleTon();
        }
        return singleTon;
    }

    public String getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(String curriculum) {
        this.curriculum = curriculum;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

}
