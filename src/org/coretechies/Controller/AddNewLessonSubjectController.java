/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.AddNewLessonSubjectDataRequest;
import org.coretechies.MessageTransporter.AddNewLessonSubjectDataResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class AddNewLessonSubjectController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        AddNewLessonSubjectDataRequest addNewLessonSubjectDataRequest = (AddNewLessonSubjectDataRequest) messageRequest;
        AddNewLessonSubjectDataResponse addNewLessonSubjectDataResponse = new AddNewLessonSubjectDataResponse();
        
        addNewLessonSubjectDataResponse.setMessageType("AddNewLessonSubjectResponse");

        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

//            String query = "Select * from ";
            String query = "SELECT subject FROM topic_grade left join topic on topic.topic_id = topic_grade.Topic_Id where curriculum = \'"+addNewLessonSubjectDataRequest.getCurriculum()+"\' and grade = \'"+addNewLessonSubjectDataRequest.getGrade()+"\'";
            System.out.println("Query : "+ query);
            ResultSet rs = stmt.executeQuery(query);

            ArrayList<String> arrayList = new ArrayList<String>();
            while(rs.next())
                arrayList.add(rs.getString("subject"));
            
            addNewLessonSubjectDataResponse.setSubjectList(arrayList);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                addNewLessonSubjectDataResponse.setHash(rs.getString("SHA"));
            
            return addNewLessonSubjectDataResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
