/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.LessonResponse;
import org.coretechies.MessageTransporter.LessonRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.SchoolDistrictRequest;
import org.coretechies.MessageTransporter.SchoolDistrictResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class SchoolDistrictController {

    private SingleTon singleTon;
    
    public MessageResponse getData(MessageRequest messageRequest, String IP){
     
        SchoolDistrictRequest schoolDistrictRequest = (SchoolDistrictRequest) messageRequest;
        SchoolDistrictResponse schoolDistrictResponse = new SchoolDistrictResponse();
        schoolDistrictResponse.setMessageType("SchoolDistrictResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT school_district FROM school_district where state = (select code from state where state = \'"+schoolDistrictRequest.getState()+"\' and country_Id = (select country_id from country where country = \'"+schoolDistrictRequest.getCountry()+"\')) and category = \'"+schoolDistrictRequest.getSchoolCategory()+"\' and zip between "+(schoolDistrictRequest.getZIP()-10)+" and "+(schoolDistrictRequest.getZIP()+10);
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            ArrayList<String> DistrictList = new ArrayList<>();
            while(rs.next())
                DistrictList.add(rs.getString("school_district"));
            
            schoolDistrictResponse.setSchoolDistrictList(DistrictList);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                schoolDistrictResponse.setHash(rs.getString("SHA"));
            return schoolDistrictResponse;            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    
    public int checkValidation(MessageRequest messageRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(messageRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }
    
}
