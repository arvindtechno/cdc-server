/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.CreateUserRequest;
import org.coretechies.MessageTransporter.CreateUserResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class CreateUserController {
    
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        CreateUserResponse createUserResponse = new CreateUserResponse();
        createUserResponse.setMessageType("CreateUserResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT * FROM country";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            ArrayList<String> countryList = new ArrayList<>();
            while(rs.next())
                countryList.add(rs.getString("country"));
            createUserResponse.setCountryList(countryList);
            
            query = "SELECT * FROM state where country_id = (select country_id from country where country= 'United States of America')";
            System.out.println("Query : "+query);
            rs = stmt.executeQuery(query);
            ArrayList<String> stateList = new ArrayList<>();
            while(rs.next()){
                System.out.println("Anand");
                stateList.add(rs.getString("state"));
            }
            createUserResponse.setStateList(stateList);
            
            query = "SELECT * FROM school_category";
            System.out.println("Query : "+query);
            rs = stmt.executeQuery(query);
            ArrayList<String> schoolCategaryList = new ArrayList<>();
            while(rs.next())
                schoolCategaryList.add(rs.getString("category"));
            createUserResponse.setSchoolCategoryList(schoolCategaryList);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                createUserResponse.setHash(rs.getString("SHA"));
            
            return createUserResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    
    public int checkValidation(MessageRequest messageRequest, String IP) {
        
        CreateUserRequest createUserRequest = (CreateUserRequest) messageRequest;
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(createUserRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }
}
