/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.CreateUserSubmitRequest;
import org.coretechies.MessageTransporter.CreateUserSubmitResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class CreateUserSubmitController {

    public MessageResponse getData(MessageRequest messageRequest, String IP){
     
        CreateUserSubmitRequest createUserSubmitRequest = (CreateUserSubmitRequest) messageRequest;
        CreateUserSubmitResponse createUserSubmitResponse = new CreateUserSubmitResponse();
        createUserSubmitResponse.setMessageType("CreateUserSubmitResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            if(createUserSubmitRequest.getMessageType().equals("CreateSchoolUserSubmitRequest")){
                String query = "Select code from country where country = \'"+createUserSubmitRequest.getCountryName()+"\'";
                System.out.println("Query : "+query);
                ResultSet rs = stmt.executeQuery(query);

                if(rs.next()){
                    query = "INSERT INTO `cdc_beta`.`user_info` (`username`, `PASSWORD`, `first_name`, `last_name`, `access_type`, `user_group`, `country`) VALUES (\'"+createUserSubmitRequest.getUserName()+"\', \'"+createUserSubmitRequest.getPassword()+"\', \'"+createUserSubmitRequest.getFirstName()+"\', \'"+createUserSubmitRequest.getLastName()+"\', \'"+createUserSubmitRequest.getUserType()+"\', '5', \'"+rs.getString("code")+"\')";
                    System.out.println("Query : "+query);
                    int i = stmt.executeUpdate(query);
                    query = "select last_insert_id() as last from user_info";
                    rs = stmt.executeQuery(query);

                    if(rs.next()){
                        String id = rs.getString("last");
                        query = "Select school_district_id from school_district where school_name = \'"+createUserSubmitRequest.getSchoolName()+"\'";
                        System.out.println("Query : "+query);
                        rs = stmt.executeQuery(query);
                        if(rs.next()){
                            String districtId = rs.getString("school_district_id");
                            query = "select userName from session where IPAddress = \""+IP+"\"";
                            System.out.println("Query : "+query);
                            rs = stmt.executeQuery(query);
                            if(rs.next()){
                                String userName = rs.getString("userName");
                                query = "INSERT INTO `cdc_beta`.`school_user` (`user_id`, `school_name`, `school_district_Id`, `created_by`, `updated_by`, `show_reminder`) VALUES "
                                        + "(\'"+id+"\', \'"+createUserSubmitRequest.getSchoolName()+"\', \'"+districtId+"\', \'"+userName+"\', \'"+userName+"\', 'yes')";
                                System.out.println("Query : "+query);
                                i = stmt.executeUpdate(query);

                                query = "INSERT INTO `cdc_beta`.`user_schedule` (`user_id`, `Monday`, `Tuesday`, `Wednesday`, `Thursday`, `Friday`, `created_by`, `updated_by`) VALUES (\'"+id+"\', '0', '0', '0', '0', '0', \'"+userName+"\', \'"+userName+"\')";
                                System.out.println("Query : "+query);
                                i = stmt.executeUpdate(query);
                                createUserSubmitResponse.setResponse(true);
                            }else
                                createUserSubmitResponse.setResponse(false);
                            
                        }else
                            createUserSubmitResponse.setResponse(false);
                    }else
                        createUserSubmitResponse.setResponse(false);
                }else
                    createUserSubmitResponse.setResponse(false);
            }else{
                String query = "INSERT INTO `cdc_beta`.`user_info` (`username`, `PASSWORD`, `first_name`, `last_name`, `access_type`, `user_group`) VALUES (\'"+createUserSubmitRequest.getUserName()+"\', \'"+createUserSubmitRequest.getPassword()+"\', \'"+createUserSubmitRequest.getFirstName()+"\', \'"+createUserSubmitRequest.getLastName()+"\', \'"+createUserSubmitRequest.getUserType()+"\', '5')";
                System.out.println("Query : "+query);
                int i = stmt.executeUpdate(query);
                if(i == 1)
                    createUserSubmitResponse.setResponse(true);
                else
                    createUserSubmitResponse.setResponse(false);
            }
            
            String query = "select SHA from session where IPAddress = \'"+IP+"\'";
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next())
                createUserSubmitResponse.setHash(rs.getString("SHA"));
            
            return createUserSubmitResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int checkValidation(MessageRequest messageRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(messageRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

}
