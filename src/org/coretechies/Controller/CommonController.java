/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.LogOutRequest;
import org.coretechies.MessageTransporter.LoginRequest;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class CommonController {

    public MessageRequest checkSession(MessageRequest messageRequest){
        LogOutRequest logOutRequest = new LogOutRequest();
        Timestamp timestamp = new Timestamp(new Date().getTime());
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "Select * from session";
            System.out.println("quesry : "+query);
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next()){
                if((new Date().getTime() - rs.getTimestamp("requestTime").getTime())<300000){
                    System.out.println(""+(new Date().getTime() - rs.getTimestamp("requestTime").getTime()));
                    query = "UPDATE `cdc_beta`.`session` SET `requestTime`=\'"+timestamp+"\' WHERE `sessionId`=\'"+rs.getInt("sessionId")+"\';";
                    System.out.println("quesry : "+query);
                    int i = stmt.executeUpdate(query);
                    if(i==1){
                        return messageRequest;
                    }
                }
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        logOutRequest.setMessageType("SessionLogOutRequest");
        logOutRequest.setHash(messageRequest.getHash());
        return logOutRequest;
    }
}