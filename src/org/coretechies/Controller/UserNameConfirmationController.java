/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.AddNewLessonChapterDataRequest;
import org.coretechies.MessageTransporter.AddNewLessonChapterDataResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.UserNameConfirmationRequest;
import org.coretechies.MessageTransporter.UserNameConfirmationResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class UserNameConfirmationController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        UserNameConfirmationRequest userNameConfirmationRequest = (UserNameConfirmationRequest) messageRequest;
        UserNameConfirmationResponse userNameConfirmationResponse = new UserNameConfirmationResponse();
        
        userNameConfirmationResponse.setMessageType("UserNameConfirmationResponse");

        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT username FROM cdc_beta.user_info where username  = \'"+userNameConfirmationRequest.getUserName()+"\'";
            System.out.println("Query : "+ query);
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next())
                userNameConfirmationResponse.setUserNameVarify(false);
            else
                userNameConfirmationResponse.setUserNameVarify(true);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                userNameConfirmationResponse.setHash(rs.getString("SHA"));
            
            return userNameConfirmationResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
