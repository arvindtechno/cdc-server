/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.SearchRequest;
import org.coretechies.MessageTransporter.SearchResponse;
import org.coretechies.MessageTransporter.SearchResponseData;
import org.coretechies.MessageTransporter.SearchSortedRequest;
import org.coretechies.MessageTransporter.SearchSortedResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class SearchSortedDataController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        SearchSortedRequest searchSortedRequest = (SearchSortedRequest) messageRequest;
        SearchSortedResponse searchSortedResponse = new SearchSortedResponse();
        
        searchSortedResponse.setMessageType("SearchSortedDataRequest");

        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT * FROM topic_grade left join topic on topic.topic_id = topic_grade.Topic_Id where topic_grade.keywords = \'"+searchSortedRequest.getKeyword()+"\' and topic_grade.curriculum = \'"+searchSortedRequest.getCurriculum()+"\' and topic_grade.grade = \'"+searchSortedRequest.getGrade()+"\' and topic.subject = \'"+searchSortedRequest.getSubject()+"\'";
            System.out.println("Query : "+ query);
            ResultSet rs = stmt.executeQuery(query);

            ArrayList<SearchResponseData> arrayList = new ArrayList<>();
            while(rs.next()){
                SearchResponseData response = new SearchResponseData();
                response.setSubject(rs.getString("subject"));
                response.setChapter(rs.getString("chapter"));
                response.setLesson(rs.getString("lesson"));
                response.setTopic(rs.getString("topic"));
                response.setGrade(rs.getString("grade"));
                response.setCurriculum(rs.getString("curriculum"));
                arrayList.add(response);
            }
            searchSortedResponse.setSearchResponseDatas(arrayList);

            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                searchSortedResponse.setHash(rs.getString("SHA"));
            
            return searchSortedResponse;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
