/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.DeleteSchoolHolidayRequest;
import org.coretechies.MessageTransporter.DeleteSchoolHolidayResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class DeleteSchoolHolidayController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        DeleteSchoolHolidayRequest deleteSchoolHolidayRequest = (DeleteSchoolHolidayRequest) messageRequest;
        DeleteSchoolHolidayResponse deleteSchoolHolidayResponse = new DeleteSchoolHolidayResponse();
        
        deleteSchoolHolidayResponse.setMessageType("DeleteSchoolHolidayResponse");

        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "delete from school_holiday where school_holiday_id = "+deleteSchoolHolidayRequest.getId();
            int i = stmt.executeUpdate(query);
            if(i==0)
               deleteSchoolHolidayResponse.setResponse(false);
            else
                deleteSchoolHolidayResponse.setResponse(true);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next())
                deleteSchoolHolidayResponse.setHash(rs.getString("SHA"));
            
            return deleteSchoolHolidayResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
