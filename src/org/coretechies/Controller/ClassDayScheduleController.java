/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.ClassDayScheduleRequest;
import org.coretechies.MessageTransporter.ClassDayScheduleResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class ClassDayScheduleController {

    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        ClassDayScheduleResponse classDayScheduleResponse = new ClassDayScheduleResponse();
        classDayScheduleResponse.setMessageType("ClassDayScheduleResponse");
        
        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "select * from user_schedule where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\'))";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);

            ArrayList<Integer> integers = new ArrayList<>();
            if(rs.next()){
                integers.add(rs.getInt("Monday"));
                integers.add(rs.getInt("Tuesday"));
                integers.add(rs.getInt("Wednesday"));
                integers.add(rs.getInt("Thursday"));
                integers.add(rs.getInt("Friday"));
            }
            else{
                integers.add(0);
                integers.add(0);
                integers.add(0);
                integers.add(0);
                integers.add(0);
            }
            
            classDayScheduleResponse.setScheduleList(integers);
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                classDayScheduleResponse.setHash(rs.getString("SHA"));
            
            return classDayScheduleResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(ClassDayScheduleRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

}
