/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.UpdateLeaveRequest;
import org.coretechies.MessageTransporter.UpdateLeaveResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class UpdateLeaveController {

    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        UpdateLeaveRequest updateLeaveRequest = (UpdateLeaveRequest) messageRequest;
        UpdateLeaveResponse updateLeaveResponse = new UpdateLeaveResponse();
        updateLeaveResponse.setMessageType("UpdateLeaveResponse");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            Date date = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            String dateString = year+"/"+(month+1)+"/"+day;

            String query = "select * from user_info where username = (select userName from session where IPAddress = \'"+IP+"\')";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next()){
                String id = rs.getString("user_id");
                String userName = rs.getString("username");
                query = "SELECT * FROM lesson_plan WHERE user_id = \'"+id+"\' AND classdate=\'"+updateLeaveRequest.getLeaveDate()+"\'";
                System.out.println("Query : "+query);
                rs = stmt.executeQuery(query);
                if(rs.next()){
                    updateLeaveResponse.setInterrupt(true);
                    updateLeaveResponse.setSubject(rs.getString("subject"));
                    updateLeaveResponse.setChapter(rs.getString("chapter"));
                    updateLeaveResponse.setLesson(rs.getString("lesson"));
                    updateLeaveResponse.setTopic(rs.getString("topic"));
                }else{
                    query = "UPDATE `cdc_beta`.`user_leave` SET `holiday`=\'"+updateLeaveRequest.getLeaveDate()+"\', `comments`=\'"+updateLeaveRequest.getComment()+"\', updated_by = \'"+userName+"\', `updated_on`=\'"+dateString+"\' WHERE `user_leave_id`=\'"+updateLeaveRequest.getId()+"\'";
                    System.out.println("Query : "+query);
                    int i = stmt.executeUpdate(query);
                    if(i==0)
                        updateLeaveResponse.setResponse(false);
                    else
                        updateLeaveResponse.setResponse(true);                    
                    updateLeaveResponse.setInterrupt(false);
                }
                query = "select SHA from session where IPAddress = \'"+IP+"\'";
                rs = stmt.executeQuery(query);
                if(rs.next())
                    updateLeaveResponse.setHash(rs.getString("SHA"));
            }
            return (MessageResponse)updateLeaveResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

}
