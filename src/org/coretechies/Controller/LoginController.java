/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.LoginRequest;
import org.coretechies.MessageTransporter.LoginResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;
import org.coretechies.Server.SHA;

/**
 *
 * @author Tiwari
 */
public class LoginController {

    public int checkValidation(LoginRequest loginRequest){
        String userName = loginRequest.getUserName();
        String shaFound;
        String password;
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();        
            Statement stmt = conn.createStatement();
            String query = "select * from session where userName = \'"+loginRequest.getUserName()+"\'";
            System.out.println("query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            
            if(!rs.next()){
                query = "select * from user_info where username=\""+userName+"\"";
                System.out.println("query : "+query);
                rs = stmt.executeQuery(query);
                if(rs.next())
                    password = rs.getString("password");
                else
                    return 0;
                SHA shaString = new SHA();
                shaFound = shaString.getSha(userName+password);
                if(shaFound.equals(loginRequest.getHash()))
                    return 1;
                else
                    return 0;
            }else
                return 2;
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(LoginRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return 0;
    }
    
    public MessageResponse getResponse(MessageRequest messageRequest, int resp, String IP){
        
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setResponse(resp);
        loginResponse.setMessageType("LoginResponse");
        
        SHA sha = new SHA();
        String time = System.currentTimeMillis()+"";
        time = sha.getSha(time);
        loginResponse.setHash(time);
        
        if(resp==1){
            Connection conn = null;
            try {            
                conn = DBConnect.getConnection();
                Statement stmt = conn.createStatement();
                
                String query = "Insert into session(userName, IPAddress, SHA) values(\'"+((LoginRequest)messageRequest).getUserName()+"\',\'"+IP+"\',\'"+time+"\')";
                int i = DBConnect.insertData(query);
                System.out.println("I : "+i);
                
                query = "SELECT * FROM user_info where username=\""+((LoginRequest)messageRequest).getUserName()+"\"";
                System.out.println("Query : "+query);
                ResultSet rs = stmt.executeQuery(query);
                if(rs.next()){
                    loginResponse.setFirstName(rs.getString("first_name"));
                    loginResponse.setLastName(rs.getString("last_name"));

                    Date date = new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    int year = cal.get(Calendar.YEAR);
                    int month = cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    String dateString = year+"/"+(month+1)+"/"+day;

                    query = "SELECT * FROM cdc_beta.lesson_plan where classdate= \'"+dateString+"\'";
                    System.out.println("Query : "+query);
                    rs = stmt.executeQuery(query);
                    if(rs.next()){
                        System.out.println("Available");
                        loginResponse.setCheckLessonPlan(true);
                    }else
                        loginResponse.setCheckLessonPlan(false);
                }
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println("Exception");
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return loginResponse;
    }
}