/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.SchoolRequest;
import org.coretechies.MessageTransporter.SchoolResponse;
import org.coretechies.MessageTransporter.UserNameVerificationRequest;
import org.coretechies.MessageTransporter.UserNameVerificationResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class UserVerificationController {

    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        UserNameVerificationRequest userNameVerificationRequest = (UserNameVerificationRequest) messageRequest;
        UserNameVerificationResponse userNameVerificationResponse = new UserNameVerificationResponse();
        
        userNameVerificationResponse.setMessageType("UserVerificationResponse");

        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT * FROM user_info where userName = \'"+userNameVerificationRequest.getUserName()+"\'";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next())
                userNameVerificationResponse.setVerify(false);
            else
                userNameVerificationResponse.setVerify(true);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                userNameVerificationResponse.setHash(rs.getString("SHA"));
            
            return userNameVerificationResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(UserNameVerificationRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

}
