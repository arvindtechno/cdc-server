/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.CreateUserSubmitRequest;
import org.coretechies.MessageTransporter.CreateUserSubmitResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.UpdateUserSubmitRequest;
import org.coretechies.MessageTransporter.UpdateUserSubmitResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class UpdateUserSubmitController {

    public MessageResponse getData(MessageRequest messageRequest, String IP){
     
        UpdateUserSubmitRequest updateUserSubmitRequest = (UpdateUserSubmitRequest) messageRequest;
        UpdateUserSubmitResponse updateUserSubmitResponse = new UpdateUserSubmitResponse();
        updateUserSubmitResponse.setMessageType("UpdateUserSubmitResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            if(updateUserSubmitRequest.getMessageType().equals("UpdateSchoolUserSubmitRequest")){
             
                String query = "Select code from country where country = \'"+updateUserSubmitRequest.getCountryName()+"\'";
                System.out.println("Query : "+query);
                ResultSet rs = stmt.executeQuery(query);

                if(rs.next()){
    
                    query = "UPDATE `cdc_beta`.`user_info` SET `username`=\'"+updateUserSubmitRequest.getUserName()+"\', `PASSWORD`=\'"+updateUserSubmitRequest.getPassword()+"\', `first_name`=\'"+updateUserSubmitRequest.getFirstName()+"\', `last_name`=\'"+updateUserSubmitRequest.getLastName()+"\', `access_type`=\'"+updateUserSubmitRequest.getUserType()+"\', `country`=\'"+rs.getString("code")+"\' WHERE `user_id`=\'"+updateUserSubmitRequest.getUserId()+"\'";
                    System.out.println("Query : "+query);
                    int i = stmt.executeUpdate(query);
                    query = "select school_user_id as last from school_user WHERE `user_id`=\'"+updateUserSubmitRequest.getUserId()+"\'";
                    rs = stmt.executeQuery(query);

                    if(rs.next()){
                        String id = rs.getString("last");
                        query = "Select school_district_id from school_district where school_name = \'"+updateUserSubmitRequest.getSchoolName()+"\'";
                        System.out.println("Query : "+query);
                        System.out.println("iddddddddd : "+id);
                        rs = stmt.executeQuery(query);
                        if(rs.next()){
                            String districtId = rs.getString("school_district_id");
                            query = "select userName from session where IPAddress = \""+IP+"\"";
                            System.out.println("Query : "+query);
                            rs = stmt.executeQuery(query);
                            if(rs.next()){

                                Date date = new Date();
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(date);
                                int year = cal.get(Calendar.YEAR);
                                int month = cal.get(Calendar.MONTH);
                                int day = cal.get(Calendar.DAY_OF_MONTH);
                                String dateString = year+"/"+(month+1)+"/"+day;

                                query = "UPDATE `cdc_beta`.`school_user` SET `school_name`=\'"+updateUserSubmitRequest.getSchoolName()+"\', `school_district_Id`=\'"+districtId+"\', `updated_by`= \'"+rs.getString("userName")+"\', `updated_on`=\'"+dateString+"\' WHERE `school_user_id`=\'"+id+"\'";

                                System.out.println("Query : "+query);
                                i = stmt.executeUpdate(query);
                                updateUserSubmitResponse.setResponse(true);
                            }else
                                updateUserSubmitResponse.setResponse(false);
                        }else
                            updateUserSubmitResponse.setResponse(false);
                    }else
                        updateUserSubmitResponse.setResponse(false);
                }else
                    updateUserSubmitResponse.setResponse(false);
            }else if(updateUserSubmitRequest.getMessageType().equals("UpdateUserSubmitRequest")){
                String query = "UPDATE `cdc_beta`.`user_info` SET `username`=\'"+updateUserSubmitRequest.getUserName()+"\', `PASSWORD`=\'"+updateUserSubmitRequest.getPassword()+"\', `first_name`=\'"+updateUserSubmitRequest.getFirstName()+"\', `last_name`=\'"+updateUserSubmitRequest.getLastName()+"\', `access_type`=\'"+updateUserSubmitRequest.getUserType()+"\' WHERE `user_id`=\'"+updateUserSubmitRequest.getUserId()+"\'";
                System.out.println("Query : "+query);
                int i = stmt.executeUpdate(query);
            }
            
            
            String query = "select SHA from session where IPAddress = \'"+IP+"\'";
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next())
                updateUserSubmitResponse.setHash(rs.getString("SHA"));
            
            return updateUserSubmitResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int checkValidation(MessageRequest messageRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(messageRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

}
/*
package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.CreateUserSubmitRequest;
import org.coretechies.MessageTransporter.CreateUserSubmitResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.UpdateUserSubmitRequest;
import org.coretechies.MessageTransporter.UpdateUserSubmitResponse;
import org.coretechies.Server.DBConnect;

public class CreateUserSubmitController{

    public MessageResponse getData(MessageRequest messageRequest, String IP){
     
        UpdateUserSubmitRequest updateUserSubmitRequest = (UpdateUserSubmitRequest) messageRequest;
        UpdateUserSubmitResponse updateUserSubmitResponse = new UpdateUserSubmitResponse();
        updateUserSubmitResponse.setMessageType("UpdateUserSubmitResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            if(updateUserSubmitRequest.getMessageType().equals("CreateUserSubmitRequest")){
                String query = "Select code from country where country = \'"+updateUserSubmitRequest.getCountryName()+"\'";
                System.out.println("Query : "+query);
                ResultSet rs = stmt.executeQuery(query);

                if(rs.next()){
    
                    query = "UPDATE `cdc_beta`.`user_info` SET `username`=\'"+updateUserSubmitRequest.getUserName()+"\', `PASSWORD`=\'"+updateUserSubmitRequest.getPassword()+"\', `first_name`=\'"+updateUserSubmitRequest.getFirstName()+"\', `last_name`=\'"+updateUserSubmitRequest.getLastName()+"\', `access_type`=\'"+updateUserSubmitRequest.getUserType()+"\', `country`=\'"+rs.getString("code")+"\' WHERE `user_id`=\'"+updateUserSubmitRequest+"\'";
                    
                    query = "INSERT INTO `cdc_beta`.`user_info` (`username`, `PASSWORD`, `first_name`, `last_name`, `access_type`, `user_group`, `country`) VALUES"
                            + " (\'"+updateUserSubmitRequest.getUserName()+"\', \'"+updateUserSubmitRequest.getPassword()+"\', \'"+updateUserSubmitRequest.getFirstName()+"\', \'"+updateUserSubmitRequest.getLastName()+"\', \'"+updateUserSubmitRequest.getUserType()+"\', '5', \'"+rs.getString("code")+"\')";
                    System.out.println("Query : "+query);
                    int i = stmt.executeUpdate(query);
                    query = "select last_insert_id() as last from user_info";
                    rs = stmt.executeQuery(query);

                    if(rs.next()){
                        String id = rs.getString("last");
                        query = "Select school_district_id from school_district where school_name = \'"+updateUserSubmitRequest.getName()+"\'";
                        System.out.println("Query : "+query);
                        System.out.println("iddddddddd : "+id);
                        rs = stmt.executeQuery(query);
                        if(rs.next()){
                            String districtId = rs.getString("school_district_id");
                            query = "select userName from session where IPAddress = \""+IP+"\"";
                            System.out.println("Query : "+query);
                            rs = stmt.executeQuery(query);
                            if(rs.next()){
                                
                                query = "UPDATE `cdc_beta`.`school_user` SET `school_name`='3D ACADEMY1', `school_district_Id`='700000000471', `updated_by`='core1', `updated_on`='0000-00-00 00:00:001' WHERE `school_user_id`='300000000039'";

                                query = "INSERT INTO `cdc_beta`.`school_user` (`user_id`, `school_name`, `school_district_Id`, `created_by`, `updated_by`, `show_reminder`) VALUES "
                                        + "(\'"+id+"\', \'"+updateUserSubmitRequest.getName()+"\', \'"+districtId+"\', \'"+rs.getString("userName")+"\', \'"+rs.getString("userName")+"\', 'yes')";
                                System.out.println("Query : "+query);
                                i = stmt.executeUpdate(query);
                                updateUserSubmitResponse.setResponse(true);
                            }else
                                updateUserSubmitResponse.setResponse(false);
                        }else
                            updateUserSubmitResponse.setResponse(false);
                    }else
                        updateUserSubmitResponse.setResponse(false);
                }else
                    updateUserSubmitResponse.setResponse(false);
            }else{
                String query = "INSERT INTO `cdc_beta`.`user_info` (`username`, `PASSWORD`, `first_name`, `last_name`, `access_type`, `user_group`) VALUES (\'"+updateUserSubmitRequest.getUserName()+"\', \'"+updateUserSubmitRequest.getPassword()+"\', \'"+updateUserSubmitRequest.getFirstName()+"\', \'"+updateUserSubmitRequest.getLastName()+"\', \'"+updateUserSubmitRequest.getUserType()+"\', '5')";
                System.out.println("Query : "+query);
                int i = stmt.executeUpdate(query);
                if(i == 1)
                    updateUserSubmitResponse.setResponse(true);
                else
                    updateUserSubmitResponse.setResponse(false);
            }
            
            String query = "select SHA from session where IPAddress = \'"+IP+"\'";
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next())
                updateUserSubmitResponse.setHash(rs.getString("SHA"));
            
            return updateUserSubmitResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int checkValidation(MessageRequest messageRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(messageRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

}
*/