/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.SubjectDataRequest;
import org.coretechies.MessageTransporter.SubjectDataResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class SubjectController {

    private SingleTon singleTon;
    public int checkValidation(SubjectDataRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    public MessageResponse getData(MessageRequest messageRequest, String IP) {
        
        SingleTon singleTon = SingleTon.getSingleTon();
        SubjectDataRequest subjectDataRequest = (SubjectDataRequest) messageRequest;
        SubjectDataResponse subjectDataResponse = new SubjectDataResponse();
        subjectDataResponse.setMessageType("SubjectDataResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT chapter FROM topic_grade left join topic on topic.topic_id = topic_grade.Topic_Id where curriculum = \'"+singleTon.getCurriculum()+"\' and grade = \'"+singleTon.getGrade()+"\' and subject = \'"+subjectDataRequest.getSubjectName()+"\'";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            ArrayList<String> arrayList = new ArrayList<>();
            while(rs.next())
                arrayList.add(rs.getString("chapter"));
            
            subjectDataResponse.setUnitList(arrayList);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                subjectDataResponse.setHash(rs.getString("SHA"));
            
            return subjectDataResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public void setSingleTonData(MessageRequest messageRequest) {
        singleTon = SingleTon.getSingleTon();
        SubjectDataRequest subjectDataRequest = (SubjectDataRequest) messageRequest;
        singleTon.setSubject(subjectDataRequest.getSubjectName());
    }    
}
