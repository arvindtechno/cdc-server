/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.ResourceRequest;
import org.coretechies.MessageTransporter.ResourceResponce;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class ResourceController {

    public MessageResponse getResponse(int id, String IP){
     
        ResourceResponce resourceResponce = new ResourceResponce();
        resourceResponce.setMessageType("ResourceResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "Select * from curriculum";
            ResultSet rs = stmt.executeQuery(query);
            ArrayList<String> list = new ArrayList<>();
            while (rs.next()) {
                list.add(rs.getString("curriculum"));
            }
            resourceResponce.setCurriculum(list);
            
            query = "Select * from grade";            
            rs = stmt.executeQuery(query);
            
            ArrayList<String> arrayList = new ArrayList<>();
            while (rs.next()) {                
                arrayList.add(rs.getString("grade"));
            }
            resourceResponce.setGrade(arrayList);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                resourceResponce.setHash(rs.getString("SHA"));
            
            return resourceResponce;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int checkValidation(ResourceRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);
            System.out.println("GET SHA : "+resourceRequest.getHash());
            
            System.out.println("UP");
            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    System.out.println("Inside SHA");
                    System.out.println("PUT SHA : "+rs.getString("SHA"));
                    return 1;
                }
                System.out.println("OutSide SHA");
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
            return 0;
        }
    }
    

}
