/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.ChapterRequest;
import org.coretechies.MessageTransporter.ChapterResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class ChapterController {

    private SingleTon singleTon;
    
    public MessageResponse getData(MessageRequest messageRequest, String IP){
     
        ChapterRequest chapterRequest = (ChapterRequest) messageRequest;
        ChapterResponse chapterResponse = new ChapterResponse();
        chapterResponse.setMessageType("ChapterDataResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT lesson FROM topic_grade left join topic on topic.topic_id = topic_grade.Topic_Id where curriculum = \'"+singleTon.getCurriculum()+"\' and grade = \'"+singleTon.getGrade()+"\' and subject = \'"+singleTon.getSubject()+"\'and chapter = \'"+chapterRequest.getUnitName()+"\'";
//            query = "SELECT chapter FROM topic_grade left join topic on topic.topic_id = topic_grade.Topic_Id where curriculum = \'"+singleTon.getCurriculum()+"\' and grade = \'"+singleTon.getGrade()+"\'";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            ArrayList<String> arrayList = new ArrayList<>();
            while(rs.next())
                arrayList.add(rs.getString("lesson"));
            
            chapterResponse.setChapterList(arrayList);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                chapterResponse.setHash(rs.getString("SHA"));
            
            return chapterResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(ChapterRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }
    
    public void setSingleTonData(MessageRequest messageRequest) {
        singleTon = SingleTon.getSingleTon();
        ChapterRequest chapterRequest = (ChapterRequest) messageRequest;
        singleTon.setChapter(chapterRequest.getUnitName());
    }
    
}
