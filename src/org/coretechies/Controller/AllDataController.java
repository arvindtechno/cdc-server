/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.coretechies.MessageTransporter.AllDataRequest;
import org.coretechies.MessageTransporter.AllDataResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class AllDataController {
    
    private SingleTon singleTon;
    
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        AllDataRequest allDataRequest = (AllDataRequest) messageRequest;
        
        AllDataResponse allDataResponse = new AllDataResponse();
        allDataResponse.setMessageType("AllDataResponse");
        
//        Connection conn = null;
//        try {
//            conn = DBConnect.getConnection();
//            Statement stmt = conn.createStatement();
//
//            String query = "SELECT subject FROM topic_grade left join topic on topic.topic_id = topic_grade.Topic_Id where curriculum = \'"+dataRequest.getCurriculum()+"\' and grade = \'"+dataRequest.getGrade()+"\'";
//            System.out.println("Query : "+query);
//            ResultSet rs = stmt.executeQuery(query);
//            ArrayList<String> arrayList = new ArrayList<>();
//            while(rs.next())
//                arrayList.add(rs.getString("subject"));
//            
//            query = "select SHA from session where IPAddress = \'"+IP+"\'";
//            rs = stmt.executeQuery(query);
//            if(rs.next())
//                dataResponse.setHash(rs.getString("SHA"));
//            
//            return dataResponse;
//            
//        } catch (SQLException | ClassNotFoundException ex) {
//            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
//        }
        return allDataResponse;
    }

    
    public int checkValidation(AllDataRequest resourceRequest, String IP) {        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    public void setSingleTonData(MessageRequest messageRequest) {
        singleTon = SingleTon.getSingleTon();
        AllDataRequest allDataRequest = (AllDataRequest) messageRequest;
        singleTon.setTopic(allDataRequest.getTopic());
    }    
}
