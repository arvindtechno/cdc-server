/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.MyActivity;
import org.coretechies.MessageTransporter.MyActivityHoliday;
import org.coretechies.MessageTransporter.SchoolHolidayRequest;
import org.coretechies.MessageTransporter.SchoolHolidayResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class SchoolHolidayController {

    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        SchoolHolidayResponse schoolHolidayResponse = new SchoolHolidayResponse();
        
        schoolHolidayResponse.setMessageType("SchoolHolidayResponse");

        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT * FROM school_holiday where school_district_Id = (select school_district_Id from school_user where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\')))";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);

            ArrayList<MyActivityHoliday> myActivityList = new ArrayList<>();
            while(rs.next()){
                MyActivityHoliday myActivity = new MyActivityHoliday();
                myActivity.setId(rs.getString("school_holiday_id"));
                myActivity.setDate(rs.getDate("holiday"));
                myActivity.setComment(rs.getString("comments"));
                myActivityList.add(myActivity);
            }

            schoolHolidayResponse.setSchoolHolidays(myActivityList);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                schoolHolidayResponse.setHash(rs.getString("SHA"));
            
            return schoolHolidayResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(SchoolHolidayRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

}
