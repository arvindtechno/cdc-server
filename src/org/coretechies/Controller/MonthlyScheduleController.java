/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.LessonPlanning;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.MonthlyScheduleRequest;
import org.coretechies.MessageTransporter.MonthlyScheduleResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class MonthlyScheduleController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        MonthlyScheduleRequest monthlyScheduleRequest = (MonthlyScheduleRequest) messageRequest;
        MonthlyScheduleResponse monthlyScheduleResponse = new MonthlyScheduleResponse();
        
        monthlyScheduleResponse.setMessageType("MonthlyScheduleResponse");

        System.out.println("Schedule CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT * FROM lesson_plan where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\')) And classdate LIKE \'%-"+monthlyScheduleRequest.getDateOfMonth()+"-%\'";
            System.out.println("Query : "+ query);
            ResultSet rs = stmt.executeQuery(query);

            ArrayList<LessonPlanning> arrayList = new ArrayList<>();
            while(rs.next()){
                LessonPlanning lessonPlanning = new LessonPlanning();
                lessonPlanning.setLesson_plan_id(rs.getInt("lesson_plan_id"));
                lessonPlanning.setSubject(rs.getString("subject"));
                lessonPlanning.setLesson(rs.getString("lesson"));
                lessonPlanning.setTopic(rs.getString("topic"));
                lessonPlanning.setClassDate(rs.getDate("classdate"));
                arrayList.add(lessonPlanning);
            }
            monthlyScheduleResponse.setLessonPlannings(arrayList);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                monthlyScheduleResponse.setHash(rs.getString("SHA"));
            
            return monthlyScheduleResponse;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
