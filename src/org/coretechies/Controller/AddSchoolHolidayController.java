/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.AddSchoolHolidayRequest;
import org.coretechies.MessageTransporter.AddSchoolHolidayResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class AddSchoolHolidayController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        AddSchoolHolidayRequest addSchoolHolidayRequest = (AddSchoolHolidayRequest) messageRequest;
        AddSchoolHolidayResponse addSchoolHolidayResponse = new AddSchoolHolidayResponse();
        
        addSchoolHolidayResponse.setMessageType("AddSchoolHolidayResponse");

        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "select userName from session where IPAddress = \'"+IP+"\'";
            ResultSet rs = stmt.executeQuery(query);        
            if(rs.next()){
                String userName = rs.getString("userName");
                query = "select * from school_user where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\'))";
                System.out.println("Query : "+query);
                rs = stmt.executeQuery(query);
                String schoolDistrictId = "";

                if(rs.next()){
                    schoolDistrictId = rs.getString("school_district_id");
                    query = "SELECT * FROM lesson_plan WHERE school_district_Id=\'"+schoolDistrictId+"\' AND classdate=\'"+addSchoolHolidayRequest.getHolidayDate()+"\'";
                    System.out.println("Query : "+query);
                    rs = stmt.executeQuery(query);
                    if(rs.next()){
                        addSchoolHolidayResponse.setInterrupt(true);
                        addSchoolHolidayResponse.setSubject(rs.getString("subject"));
                        addSchoolHolidayResponse.setChapter(rs.getString("chapter"));
                        addSchoolHolidayResponse.setLesson(rs.getString("lesson"));
                        addSchoolHolidayResponse.setTopic(rs.getString("topic"));
                    }else{
                        query = "INSERT INTO school_holiday (`school_district_Id`, `holiday`, `comments`, `created_by`, `updated_by`) VALUES (\'"+schoolDistrictId+"\', \'"+addSchoolHolidayRequest.getHolidayDate()+"\', \'"+addSchoolHolidayRequest.getComment()+"\', \'"+userName+"\', \'"+userName+"\');";
                        System.out.println("Quesr : "+query);
                        int i = stmt.executeUpdate(query);
                        if(i == 1){
                            addSchoolHolidayResponse.setAddResponse(true);
                            rs = stmt.executeQuery("select last_insert_id() as last_id from user_leave");
                            if(rs.next())
                                addSchoolHolidayResponse.setId(rs.getString("last_id"));
                        }
                        else
                            addSchoolHolidayResponse.setAddResponse(false);
                        addSchoolHolidayResponse.setInterrupt(false);
                    }
                }            
            }
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                addSchoolHolidayResponse.setHash(rs.getString("SHA"));
            
            return addSchoolHolidayResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
