/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.UpdateUser;
import org.coretechies.MessageTransporter.UpdateUserRequest;
import org.coretechies.MessageTransporter.UpdateUserResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class UpdateUserController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        UpdateUserRequest updateUserRequest = (UpdateUserRequest) messageRequest;
        UpdateUserResponse updateUserResponse = new UpdateUserResponse();
        
        updateUserResponse.setMessageType("UpdateUserResponse");

        System.out.println("User Update CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "select access_type from user_info where userName = (select userName from session where IPAddress = \'"+IP+"\')";
            System.out.println("Query : "+ query);
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next())
                if(rs.getString("access_type").equals("Admin")){
                    query = "SELECT * FROM user_info left join school_user using(user_id) WHERE school_user.school_district_id = (SELECT school_user.school_district_id FROM school_user WHERE user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\')))";
                    System.out.println("Query : "+ query);
                    rs = stmt.executeQuery(query);

                    ArrayList<UpdateUser> arrayList = new ArrayList<>();
                    while(rs.next()){
                        UpdateUser updateUser = new UpdateUser();
                        updateUser.setUserId(rs.getString("user_id"));
                        updateUser.setUserName(rs.getString("username"));
                        updateUser.setFirstName(rs.getString("first_name"));
                        updateUser.setLastName(rs.getString("last_name"));
                        updateUser.setPassword(rs.getString("PASSWORD"));
                        arrayList.add(updateUser);
                    }
                    updateUserResponse.setUpdateUsers(arrayList);
                    updateUserResponse.setAccess_type("Admin");
                }else{
                    query = "SELECT * FROM user_info left join school_user using(user_id) WHERE user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\'))";
                    System.out.println("Query : "+ query);
                    rs = stmt.executeQuery(query);

                    ArrayList<UpdateUser> arrayList = new ArrayList<>();
                    while(rs.next()){
                        UpdateUser updateUser = new UpdateUser();
                        updateUser.setUserId(rs.getString("user_id"));
                        updateUser.setUserName(rs.getString("username"));
                        updateUser.setFirstName(rs.getString("first_name"));
                        updateUser.setLastName(rs.getString("last_name"));
                        updateUser.setPassword(rs.getString("PASSWORD"));
                        arrayList.add(updateUser);
                    }
                    updateUserResponse.setUpdateUsers(arrayList);
                    updateUserResponse.setAccess_type("Guest");
                }
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                updateUserResponse.setHash(rs.getString("SHA"));
            
            return updateUserResponse;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }    
}
