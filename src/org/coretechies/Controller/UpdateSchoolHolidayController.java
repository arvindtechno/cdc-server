/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.UpdateSchoolHolidayRequest;
import org.coretechies.MessageTransporter.UpdateSchoolHolidayResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class UpdateSchoolHolidayController {

    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        UpdateSchoolHolidayRequest updateSchoolHolidayRequest = (UpdateSchoolHolidayRequest) messageRequest;
        UpdateSchoolHolidayResponse updateSchoolHolidayResponse = new UpdateSchoolHolidayResponse();
        updateSchoolHolidayResponse.setMessageType("UpdateSchoolHolidayResponse");

        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "select * from school_holiday where `school_holiday_id`=\'"+updateSchoolHolidayRequest.getId()+"\'";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            String schoolDistrictId = "";

            if(rs.next()){
                schoolDistrictId = rs.getString("school_district_id");
                query = "SELECT * FROM lesson_plan WHERE school_district_Id=\'"+schoolDistrictId+"\' AND classdate=\'"+updateSchoolHolidayRequest.getHolidayDate()+"\'";
                System.out.println("Query : "+query);
                rs = stmt.executeQuery(query);
                if(rs.next()){
                    updateSchoolHolidayResponse.setInterrupt(true);
                    updateSchoolHolidayResponse.setSubject(rs.getString("subject"));
                    updateSchoolHolidayResponse.setChapter(rs.getString("chapter"));
                    updateSchoolHolidayResponse.setLesson(rs.getString("lesson"));
                    updateSchoolHolidayResponse.setTopic(rs.getString("topic"));
                }else{
                    query = "UPDATE `cdc_beta`.`school_holiday` SET `holiday`=\'"+updateSchoolHolidayRequest.getHolidayDate()+"\', `comments`=\'"+updateSchoolHolidayRequest.getComment()+"\' WHERE `school_holiday_id`=\'"+updateSchoolHolidayRequest.getId()+"\'";
                    int i = stmt.executeUpdate(query);
                    if(i==0)
                        updateSchoolHolidayResponse.setResponse(false);
                    else
                        updateSchoolHolidayResponse.setResponse(true);
                    updateSchoolHolidayResponse.setInterrupt(false);
                }
                query = "select SHA from session where IPAddress = \'"+IP+"\'";
                rs = stmt.executeQuery(query);
                if(rs.next())
                    updateSchoolHolidayResponse.setHash(rs.getString("SHA"));
                
                
                return updateSchoolHolidayResponse;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

}
