package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MediaDataRequest;
import org.coretechies.MessageTransporter.MediaDataResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

public class MediaDataController {
    
    public MessageResponse getData(MessageRequest messageRequest, String IP){
     
        MediaDataRequest mediaDataRequest = (MediaDataRequest) messageRequest;
        MediaDataResponse dataResponse = new MediaDataResponse();
        dataResponse.setMessageType("MediaDataResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT absoluteAddress FROM cdc_beta.topic left join fileaddress using(topic_id) where topic.chapter = \'"+mediaDataRequest.getSubject()+"\' and topic.topic = \'"+mediaDataRequest.getTopic()+"\' and topic.lesson = \'"+mediaDataRequest.getLesson()+"\' and fileaddress.fileType = \'"+mediaDataRequest.getFileType()+"\'";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            if (rs.next()) {
                String path = rs.getString("absoluteAddress");
                dataResponse.setPath(path.substring(path.indexOf('.')+1));
                if(mediaDataRequest.getFileType().equals("3dmp4")){
                    dataResponse.setPath("3dmp4");
                }
            }

            System.out.println(""+dataResponse.getPath());
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                dataResponse.setHash(rs.getString("SHA"));
        
            return dataResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    
    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    public ArrayList getPath(MessageRequest messageRequest) {
        MediaDataRequest mediaDataRequest = (MediaDataRequest) messageRequest;
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            System.out.println("Modulo Id : ");
            String query = "SELECT * FROM cdc_beta.topic left join fileaddress using(topic_id) where topic.chapter = \'"+mediaDataRequest.getSubject()+"\' and topic.topic = \'"+mediaDataRequest.getTopic()+"\' and topic.lesson = \'"+mediaDataRequest.getLesson()+"\' and fileaddress.fileType = \'"+mediaDataRequest.getFileType()+"\'";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            ArrayList<Object> objects = new ArrayList<>();
            if (rs.next()) {
                String path = rs.getString("absoluteAddress");
                objects.add(rs.getString("absoluteAddress"));
                objects.add(rs.getInt("dataSize"));
                return objects;
            }
        }   
        catch (ClassNotFoundException ex) {
            Logger.getLogger(MediaDataController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MediaDataController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
