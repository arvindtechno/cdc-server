/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.AddLeaveRequest;
import org.coretechies.MessageTransporter.AddLeaveResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class AddLeaveController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        AddLeaveRequest addLeaveRequest = (AddLeaveRequest) messageRequest;
        AddLeaveResponse addLeaveResponse = new AddLeaveResponse();
        
        addLeaveResponse.setMessageType("AddLeaveResponse");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "select * from user_info where username = (select userName from session where IPAddress = \'"+IP+"\')";
            ResultSet rs = stmt.executeQuery(query);        
            if(rs.next()){
                String userName = rs.getString("userName");
                String id = rs.getString("user_id");
                query = "SELECT * FROM lesson_plan WHERE user_id = \'"+id+"\' AND classdate=\'"+addLeaveRequest.getLeaveDate()+"\'";
                System.out.println("Query : "+query);
                rs = stmt.executeQuery(query);
                if(rs.next()){
                    addLeaveResponse.setInterrupt(true);
                    addLeaveResponse.setSubject(rs.getString("subject"));
                    addLeaveResponse.setChapter(rs.getString("chapter"));
                    addLeaveResponse.setLesson(rs.getString("lesson"));
                    addLeaveResponse.setTopic(rs.getString("topic"));
                }else{
                    query = "INSERT INTO user_leave (`user_id`, `holiday`, `comments`, `created_by`, `updated_by`) VALUES (\'"+id+"\', \'"+addLeaveRequest.getLeaveDate()+"\', \'"+addLeaveRequest.getComment()+"\', \'"+userName+"\', \'"+userName+"\');";
                    System.out.println("Quesr : "+query);
                    int i = stmt.executeUpdate(query);
                    if(i == 1){
                        addLeaveResponse.setAddResponse(true);
                        rs = stmt.executeQuery("select last_insert_id() as last_id from user_leave");
                        if(rs.next())
                            addLeaveResponse.setId(rs.getInt("last_id"));
                    }
                    else
                        addLeaveResponse.setAddResponse(false);
                    addLeaveResponse.setInterrupt(false);
                }
            }
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                addLeaveResponse.setHash(rs.getString("SHA"));
            
            return addLeaveResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return addLeaveResponse;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
