/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.LessonPlanning;
import org.coretechies.MessageTransporter.LessonPlanningRequest;
import org.coretechies.MessageTransporter.LessonPlanningResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class LessonPlanningController {

    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        LessonPlanningRequest lessonPlanningRequest = (LessonPlanningRequest) messageRequest;
        LessonPlanningResponse lessonPlanningResponse = new LessonPlanningResponse();
        
        lessonPlanningResponse.setMessageType("LessonPlanningResponse");

        System.out.println("Lesson Planning CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs;
            
            String query = "SELECT * from user_schedule where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\'))";
            System.out.println("Query : "+query);
            rs = stmt.executeQuery(query);
            if(rs.next())
            {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                if((rs.getInt("Monday") + rs.getInt("Tuesday") + rs.getInt("Wednesday") + rs.getInt("Thursday") + rs.getInt("Friday"))>0){
                    query = "SELECT * FROM school_holiday where holiday like concat("+year+",'-%') AND school_district_Id = (select school_district_Id from school_user where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\')))";
                    System.out.println("Query : "+query);
                    rs = stmt.executeQuery(query);
                    if(rs.next()){

                        int month = cal.get(Calendar.MONTH);
                        int day = cal.get(Calendar.DAY_OF_MONTH);
                        String dateString = year+"/"+(month+1)+"/"+day;
                        query = "SELECT * FROM lesson_plan where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\')) and classdate >= \'"+dateString+"\'";
                        System.out.println("Query : "+query);
                        rs = stmt.executeQuery(query);
                        ArrayList<LessonPlanning> lessonPlannings = new ArrayList<>();

                        while(rs.next()){
                            LessonPlanning lessonPlanning = new LessonPlanning();
                            lessonPlanning.setLesson_plan_id(rs.getInt("lesson_plan_id"));
                            lessonPlanning.setSubject(rs.getString("subject"));
                            lessonPlanning.setLesson(rs.getString("lesson"));
                            lessonPlanning.setTopic(rs.getString("topic"));
                            lessonPlanning.setClassDate(rs.getDate("classdate"));

                            lessonPlannings.add(lessonPlanning);
                        }

                        lessonPlanningResponse.setLessonPlanningList(lessonPlannings);
                        lessonPlanningResponse.setResponse(true);
                    }else{
                        lessonPlanningResponse.setResponse(false);
                    }
                }else{
                    lessonPlanningResponse.setResponse(false);
                }
            }else{
                lessonPlanningResponse.setResponse(false);
            }
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                lessonPlanningResponse.setHash(rs.getString("SHA"));

            return lessonPlanningResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lessonPlanningResponse;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

}
