/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.LogOutRequest;
import org.coretechies.MessageTransporter.LogOutRespnse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class LogOutController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        LogOutRequest logOutRequest = (LogOutRequest) messageRequest;
        LogOutRespnse logOutRespnse = new LogOutRespnse();
        
        logOutRespnse.setMessageType("LogOutResponse");

        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "select SHA from session where IPAddress = \'"+IP+"\'";
            
            ResultSet rs;
            rs = stmt.executeQuery(query);
            if(rs.next())
                logOutRespnse.setHash(rs.getString("SHA"));
            
            query = "DELETE FROM `cdc_beta`.`session` WHERE IPAddress = \'"+IP+"\'";
            System.out.println("Query : "+query);
            int i = stmt.executeUpdate(query);
            if(i==1)
                logOutRespnse.setResponse(2);
            else
                logOutRespnse.setResponse(1);
            
            return logOutRespnse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return logOutRespnse;
    }

    public MessageResponse getSessionData(MessageRequest messageRequest, String IP){
    
        LogOutRequest logOutRequest = (LogOutRequest) messageRequest;
        LogOutRespnse logOutRespnse = new LogOutRespnse();
        
        logOutRespnse.setMessageType("SessionLogOutResponse");

        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "select SHA from session where IPAddress = \'"+IP+"\'";
            
            ResultSet rs;
            rs = stmt.executeQuery(query);
            if(rs.next())
                logOutRespnse.setHash(rs.getString("SHA"));
            
            query = "DELETE FROM `cdc_beta`.`session` WHERE IPAddress = \'"+IP+"\'";
            System.out.println("Query : "+query);
            int i = stmt.executeUpdate(query);
            if(i==1)
                logOutRespnse.setResponse(3);
            else
                logOutRespnse.setResponse(1);
            
            return logOutRespnse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return logOutRespnse;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }    
}
