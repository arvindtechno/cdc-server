/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.UpdateLessonPlanRequest;
import org.coretechies.MessageTransporter.UpdateLessonPlanResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class UpdateLessonPlanController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        UpdateLessonPlanRequest updateLessonPlanRequest = (UpdateLessonPlanRequest) messageRequest;
        UpdateLessonPlanResponse updateLessonPlanResponse = new UpdateLessonPlanResponse();
        
        updateLessonPlanResponse.setMessageType("UpdateLessonPlanResponse");

        System.out.println("Lesson Update CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
//UPDATE `cdc_beta`.`lesson_plan` SET `classdate`='2014-05-01' WHERE `lesson_plan_id`='1';

            String query = "UPDATE `cdc_beta`.`lesson_plan` SET `classdate`= \'"+updateLessonPlanRequest.getClassDate()+"\' WHERE `lesson_plan_id`= \'"+updateLessonPlanRequest.getId()+"\'";
            System.out.println("Query : "+ query);
            int i = stmt.executeUpdate(query);

            if(i==1)
                updateLessonPlanResponse.setResponse(true);
            else
                updateLessonPlanResponse.setResponse(false);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next())
                updateLessonPlanResponse.setHash(rs.getString("SHA"));
            
            return updateLessonPlanResponse;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }    
}
