/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.LoginRequest;
import org.coretechies.MessageTransporter.LoginResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;
import org.coretechies.Server.SHA;

/**
 *
 * @author Tiwari
 */
public class CheckUserController {

    public int checkValidation(String ip){
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();            
            Statement stmt = conn.createStatement();
            String query = "select * from session where IPAddress = \'"+ip+"\'";
            System.out.println("query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            
            if(rs.next()){
                return 1;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(LoginRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(CheckUserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return 0;
    }
}