/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.ChangePasswordRequest;
import org.coretechies.MessageTransporter.ChangePasswordResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class ChangePasswordController {

    public MessageResponse getData(MessageRequest messageRequest, String IP){
     
        ChangePasswordRequest changePasswordRequest = (ChangePasswordRequest) messageRequest;
        ChangePasswordResponse changePasswordResponse  = new ChangePasswordResponse();
        changePasswordResponse.setMessageType("ChangePasswordResponse");
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT password FROM user_info where username = (select userName from session where IPAddress = \""+IP+"\")";
            System.out.println("Query : "+query);
            ResultSet rs = stmt.executeQuery(query);
            
            if(rs.next()){
                if(rs.getString("password").equals(changePasswordRequest.getOldPassword())){
                    query = "UPDATE user_info SET `password`= \'"+changePasswordRequest.getNewPassword()+"\' WHERE userName = (select userName from session where IPAddress = \""+IP+"\")";
                    int i = stmt.executeUpdate(query);
                    changePasswordResponse.setChangePasswordResponse(i);
                }
            }
                
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                changePasswordResponse.setHash(rs.getString("SHA"));
            
            return changePasswordResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int checkValidation(MessageRequest messageRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(messageRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

}
