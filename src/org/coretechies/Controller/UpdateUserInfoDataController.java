/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.UpdateUserInfo;
import org.coretechies.MessageTransporter.UpdateUserInfoRequest;
import org.coretechies.MessageTransporter.UpdateUserInfoResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class UpdateUserInfoDataController {
    private String country;
    private String state;
    private String district;
    private String category;
    private int zip;
    
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        UpdateUserInfoRequest updateUserInfoRequest = (UpdateUserInfoRequest) messageRequest;
        UpdateUserInfoResponse updateUserInfoResponse = new UpdateUserInfoResponse();
        
        updateUserInfoResponse.setMessageType("UpdateUserInfoResponse");

        System.out.println("User Update CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT * FROM school_user inner join user_info using(user_id) where user_info.user_id = \'"+updateUserInfoRequest.getId()+"\'";
            System.out.println("Query : "+ query);
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next()){
                query = "SELECT ui.access_type,c.country,ui.first_name,ui.last_name,ui.username,ui.PASSWORD,ui.user_id,s.state,sd.category,sd.school_district,sd.school_name,sd.zip FROM user_info as ui left join school_user as su using(user_id) left join school_district as sd using(school_district_id) inner join state as s on s.code = sd.state inner join country as c on c.code = ui.country WHERE user_id = \'"+updateUserInfoRequest.getId()+"\'";
                System.out.println("Query : "+ query);
                rs = stmt.executeQuery(query);

                UpdateUserInfo updateUserInfo = new UpdateUserInfo();
                while(rs.next()){
                    updateUserInfo.setId(rs.getString("user_id"));
                    updateUserInfo.setUserName(rs.getString("username"));
                    updateUserInfo.setFirstName(rs.getString("first_name"));
                    updateUserInfo.setLastName(rs.getString("last_name"));
                    updateUserInfo.setPassword(rs.getString("PASSWORD"));
                    country = rs.getString("country");
                    state = rs.getString("state");
                    district = rs.getString("school_district");
                    category = rs.getString("category");
                    zip = Integer.parseInt(rs.getString("zip"));
                    updateUserInfo.setCountry(country);
                    updateUserInfo.setState(state);
                    updateUserInfo.setSchoolDistrict(district);
                    updateUserInfo.setSchoolCategary(category);
                    updateUserInfo.setSchool(rs.getString("school_name"));
                    updateUserInfo.setZip(zip+"");
                    updateUserInfo.setUserType(rs.getString("access_type"));
                }
                updateUserInfoResponse.setUpdateUserInfo(updateUserInfo);

                query = "select * from country";
                System.out.println("Query : "+ query);
                rs = stmt.executeQuery(query);
                    ArrayList<String> countryList = new ArrayList<>();
                while(rs.next())
                    countryList.add(rs.getString("country"));
                updateUserInfoResponse.setCountryList(countryList);

                query = "select * from school_category";
                System.out.println("Query : "+ query);
                rs = stmt.executeQuery(query);
                ArrayList<String> categoryList = new ArrayList<>();
                while(rs.next())
                    categoryList.add(rs.getString("category"));
                updateUserInfoResponse.setCategoryList(categoryList);

                query = "select * from state where country_id = (select country_id from country where country = \'"+country+"\')";
                System.out.println("Query : "+ query);
                rs = stmt.executeQuery(query);
                ArrayList<String> stateList = new ArrayList<>();
                while(rs.next())
                    stateList.add(rs.getString("state"));
                updateUserInfoResponse.setStateList(stateList);

                query = "select school_district FROM school_district where state = (select code from state where state = \'"+state+"\' and country_Id = (select country_id from country where country = \'"+country+"\')) and category = \'"+category+"\' and zip between "+(zip-10)+" and "+(zip+10);
                System.out.println("Query : "+ query);
                rs = stmt.executeQuery(query);
                ArrayList<String> districtList = new ArrayList<>();
                while(rs.next())
                    districtList.add(rs.getString("school_district"));
                updateUserInfoResponse.setDistrictList(districtList);

                query = "select school_name FROM school_district where state = (select code from state where state = \'"+state+"\' and country_Id = (select country_id from country where country = \'"+country+"\')) and category = \'"+category+"\' and school_district = \'"+district+"\' and zip between "+(zip-10)+" and "+(zip+10);
                System.out.println("Query : "+ query);
                rs = stmt.executeQuery(query);
                ArrayList<String> schoolList = new ArrayList<>();
                while(rs.next())
                    schoolList.add(rs.getString("school_name"));
                updateUserInfoResponse.setSchoolList(schoolList);

            }else{
                query = "SELECT access_type, country, first_name, last_name, username, PASSWORD, user_id FROM user_info WHERE user_id = \'"+updateUserInfoRequest.getId()+"\'";
                System.out.println("Query : "+ query);
                rs = stmt.executeQuery(query);

                UpdateUserInfo updateUserInfo = new UpdateUserInfo();
                while(rs.next()){
                    updateUserInfo.setId(rs.getString("user_id"));
                    updateUserInfo.setUserName(rs.getString("username"));
                    updateUserInfo.setFirstName(rs.getString("first_name"));
                    updateUserInfo.setLastName(rs.getString("last_name"));
                    updateUserInfo.setPassword(rs.getString("PASSWORD"));
                    updateUserInfo.setCountry("USA");
                    updateUserInfo.setUserType(rs.getString("access_type"));
                }
                updateUserInfoResponse.setUpdateUserInfo(updateUserInfo);
            }
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                updateUserInfoResponse.setHash(rs.getString("SHA"));

            return updateUserInfoResponse;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return updateUserInfoResponse;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }   
}