/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.AddLeaveRequest;
import org.coretechies.MessageTransporter.AddLeaveResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.MessageTransporter.UpdateClassDayScheduleRequest;
import org.coretechies.MessageTransporter.UpdateClassDayScheduleResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class UpdateClassDayScheduleController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        UpdateClassDayScheduleRequest updateClassDayScheduleRequest = (UpdateClassDayScheduleRequest) messageRequest;
        UpdateClassDayScheduleResponse updateClassDayScheduleResponse = new UpdateClassDayScheduleResponse();
        
        updateClassDayScheduleResponse.setMessageType("UpdateClassDayScheduleResponse");

        System.out.println("User Name Verification CONTROLLER");
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            ArrayList<Integer> arrayList = updateClassDayScheduleRequest.getScheduleList();
            String query = "UPDATE user_schedule SET `Monday`=\'"+arrayList.get(0)+"\', `Tuesday`=\'"+arrayList.get(1)+"\', `Wednesday`=\'"+arrayList.get(2)+"\', `Thursday`=\'"+arrayList.get(3)+"\', `Friday`=\'"+arrayList.get(4)+"\' where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\'))";
            int i = stmt.executeUpdate(query);

            if(i==1)
                updateClassDayScheduleResponse.setResponse(true);
            else
                updateClassDayScheduleResponse.setResponse(false);
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            ResultSet rs = stmt.executeQuery(query);
            if(rs.next())
                updateClassDayScheduleResponse.setHash(rs.getString("SHA"));
            
            return updateClassDayScheduleResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
