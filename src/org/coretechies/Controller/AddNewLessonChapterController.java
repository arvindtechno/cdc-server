/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.MessageTransporter.AddNewLessonChapterDataRequest;
import org.coretechies.MessageTransporter.AddNewLessonChapterDataResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;
import org.coretechies.Server.DBConnect;

/**
 *
 * @author Tiwari
 */
public class AddNewLessonChapterController {
    public MessageResponse getData(MessageRequest messageRequest, String IP){
    
        AddNewLessonChapterDataRequest addNewLessonChapterDataRequest = (AddNewLessonChapterDataRequest) messageRequest;
        AddNewLessonChapterDataResponse addNewLessonChapterDataResponse = new AddNewLessonChapterDataResponse();
        
        addNewLessonChapterDataResponse.setMessageType("AddNewLessonChapterDataResponse");

        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();

            String query = "SELECT chapter FROM topic_grade left join topic on topic.topic_id = topic_grade.Topic_Id where curriculum = \'"+addNewLessonChapterDataRequest.getCurriculum()+"\' and grade = \'"+addNewLessonChapterDataRequest.getGrade()+"\' and subject = \'"+addNewLessonChapterDataRequest.getSubject()+"\'";
            System.out.println("Query : "+ query);
            ResultSet rs = stmt.executeQuery(query);

            ArrayList<String> arrayList = new ArrayList<>();
            while(rs.next()){
                arrayList.add(rs.getString("chapter"));
            }
            addNewLessonChapterDataResponse.setChapterList(arrayList);
            
            query = "select * from user_schedule where user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\'))";
            System.out.println("Query : "+query);
            rs = stmt.executeQuery(query);

            ArrayList<Integer> integers = new ArrayList<>();
            if(rs.next()){
                integers.add(rs.getInt("Monday"));
                integers.add(rs.getInt("Tuesday"));
                integers.add(rs.getInt("Wednesday"));
                integers.add(rs.getInt("Thursday"));
                integers.add(rs.getInt("Friday"));
            }
            else{
                integers.add(0);
                integers.add(0);
                integers.add(0);
                integers.add(0);
                integers.add(0);
            }
            addNewLessonChapterDataResponse.setClassDayScheduleList(integers);
            
            Calendar cal = Calendar.getInstance();
            query = "SELECT * FROM cdc_beta.lesson_plan where classdate >= \'"+cal.get(Calendar.YEAR)+"/"+(cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.DATE)+"\' And user_id = (select user_id from user_info where username = (select userName from session where IPAddress = \'"+IP+"\'))";
            System.out.println("Query : "+query);
            rs = stmt.executeQuery(query);

            ArrayList<Date> dates = new ArrayList<>();
            while(rs.next()){
                dates.add(rs.getDate("classDate"));
            }
            addNewLessonChapterDataResponse.setDateList(dates);
            
            query = "select SHA from session where IPAddress = \'"+IP+"\'";
            rs = stmt.executeQuery(query);
            if(rs.next())
                addNewLessonChapterDataResponse.setHash(rs.getString("SHA"));
            
            return addNewLessonChapterDataResponse;
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ResourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int checkValidation(MessageRequest resourceRequest, String IP) {
        
        Connection conn = null;
        try {
            conn = DBConnect.getConnection();
            Statement stmt = conn.createStatement();
            String query = "SELECT * from session where IPAddress = \""+IP+"\"";
            ResultSet rs = stmt.executeQuery(query);

            if(rs.next()){
                if(rs.getString("SHA").equals(resourceRequest.getHash())){
                    return 1;
                }
                return 0;
            }
            return 0;
        } catch (ClassNotFoundException | SQLException ex) {
            return 0;
        }
    }

    
}
