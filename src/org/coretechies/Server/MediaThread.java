/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.Controller.MediaDataController;
import org.coretechies.MessageTransporter.Data;
import org.coretechies.MessageTransporter.MediaDataRequest;
import org.coretechies.MessageTransporter.MediaDataResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;

/**
 *
 * @author Tiwari
 */
public class MediaThread implements Runnable{

    private MessageRequest messageRequest;
    private String IP;
    private MessageResponse messageResponse;
    private ObjectOutputStream objectOutputStream;

    public MessageRequest getMessageRequest() {
        return messageRequest;
    }

    public void setMessageRequest(MessageRequest messageRequest) {
        this.messageRequest = messageRequest;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }
    
    public ObjectOutputStream getObjectOutputStream() {
        return objectOutputStream;
    }

    public void setObjectOutputStream(ObjectOutputStream objectOutputStream) {
        this.objectOutputStream = objectOutputStream;
    }
    
    @Override
    public void run() {
        while(messageRequest==null || IP == null){
            try {
                System.out.println("Sleep");
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(MediaThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Starttttt");
            
        MediaDataController dataController = new MediaDataController();
        int validation = dataController.checkValidation((MediaDataRequest)messageRequest, IP);
        if(validation==1){
            try {
                messageResponse = dataController.getData(messageRequest, IP);
                messageResponse.setMessageType("MediaDataResponse");
                objectOutputStream.writeObject(messageResponse);
                objectOutputStream.flush();
                String path = ((MediaDataResponse)messageResponse).getPath();
                System.out.println("FFLUSH");
                Data data = new Data();
                ArrayList list = dataController.getPath(messageRequest);
                final File file = new File((String)list.get(0));
                byte[] bs = new byte[127];
                if(!file.exists()) {
                    bs = "File Not Found ".getBytes();
                    data.setHash(messageResponse.getHash());
                    data.setMessageType("MediaData"+path);
                    data.setBs(bs);
                    objectOutputStream.writeObject((MessageResponse)data);
                    objectOutputStream.writeObject(null);
                    objectOutputStream.flush();
                    objectOutputStream.reset();
                }
                else{
                    String dataPath = path;
                    try {
                        FileInputStream fileInputStream = new FileInputStream(file);
                        long size = file.length();
                        int len;
                        while((len = fileInputStream.read(bs))!=-1){
                            data = new Data();
                            data.setHash(messageResponse.getHash());
                            data.setSequenceNumber(size);
                            data.setMessageType("MediaData"+dataPath);
                            data.setBs(bs);
                            objectOutputStream.writeObject((MessageResponse)data);
                            objectOutputStream.flush();
                            objectOutputStream.reset();
                            bs = new byte[1024];
                        }
                        data.setHash(messageResponse.getHash());
                        data.setMessageType("MediaData"+dataPath);
                        data.setBs("\nEND".getBytes());
                        objectOutputStream.writeObject((MessageResponse)data);
                        objectOutputStream.flush();
                        objectOutputStream.reset();
                    } catch (IOException ex) {
                        Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(MediaThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
