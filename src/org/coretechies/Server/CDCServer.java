/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.coretechies.Server;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
public class CDCServer extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        try {
            
            AnchorPane page = (AnchorPane) FXMLLoader.load(getClass().getResource("front"
                    + ".fxml"));
             Scene scene = new Scene(page);
            
            primaryStage.setScene(scene);
            primaryStage.fullScreenProperty();
            primaryStage.setTitle("CDC");
            primaryStage.show();
     
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
