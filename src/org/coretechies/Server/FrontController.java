
package org.coretechies.Server;

import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

public class FrontController implements Initializable {
    @FXML
    private Button start;
    @FXML
    private Button stop;
    CoreServer coreServer;
    int flag;
    Thread server;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        flag = 0;
    }    

    @FXML
    private void start(ActionEvent event) {
        if(flag!=1){
            try {
                coreServer = CoreServer.getServer();
                if(coreServer==null)
                    return;
                System.out.println("Start");
                coreServer.setRunning(true);
                server = new Thread(coreServer);
                server.start();
            } catch (IOException ex) {
                Logger.getLogger(FrontController.class.getName()).log(Level.SEVERE, null, ex);
            }
            flag = 1;
            start.setText("Running...");
            start.setDisable(true);
        }
    }

    @FXML
    private void stop(ActionEvent event) {      
            coreServer.setRunning(false);       
            flag = 0;
            start.setText("Start");
            start.setDisable(false);
    }
    
}
