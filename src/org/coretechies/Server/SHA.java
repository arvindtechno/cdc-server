package org.coretechies.Server;

import java.io.Serializable;
import java.security.MessageDigest;

public class SHA implements Serializable{
    
    public String getSha(String datauser){
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(datauser.getBytes());

            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            StringBuffer hexString = new StringBuffer();
            for (int i=0;i<byteData.length;i++) {
                String hex=Integer.toHexString(0xff & byteData[i]);
                if(hex.length()==1) hexString.append('0');
                    hexString.append(hex);
            }
            return hexString.toString();
        }
        catch(Exception eee)
        {
            eee.printStackTrace();
        }
        return null;
    }
    
}