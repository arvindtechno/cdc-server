
package org.coretechies.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CoreServer extends ServerSocket implements Runnable{
    
    final private static int PORT = 5051;
    final private static int BACKLOG = 1000;
    boolean running;
    Socket socket;
    
    public static CoreServer getServer() throws IOException{
        return new CoreServer();
    }
	
    private CoreServer() throws IOException{
        super(PORT,BACKLOG);
        System.out.println(""+this.getInetAddress());
        running = false;
    }

    @Override
    public void close() throws IOException {
        System.out.println("Closing Socket");
        super.close();
    }  

    synchronized public boolean isRunning() {
        return running;
    }

    synchronized public void setRunning(boolean running) {
        if(!running) try {
            this.close();
        } catch (IOException ex) {
            Logger.getLogger(CoreServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.running = running;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            while(running){
                System.out.println(running);
                System.out.println("Waiting.......");
                socket = this.accept();
                System.out.println("Request Accepted.....");
                Client client = new Client(socket);
            }
            System.out.println("Stopping Server");
        } catch (IOException ex) {
            Logger.getLogger(CoreServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}