package org.coretechies.Server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.Controller.MediaDataController;
import org.coretechies.MessageTransporter.Data;
import org.coretechies.MessageTransporter.MediaDataContinueRequest;
import org.coretechies.MessageTransporter.MediaDataRequest;
import org.coretechies.MessageTransporter.MediaDataResponse;
import org.coretechies.MessageTransporter.MessageRequest;
import org.coretechies.MessageTransporter.MessageResponse;

public class ClientUnity3d implements Runnable{

    Socket socket;
    String IP;
    File file;
    int num=0;
    String hash = null;
    
    ClientUnity3d(Socket s) {
        this.socket = s;
        InetAddress address = this.socket.getInetAddress();
        this.IP = address.getHostAddress();
        Thread t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        ObjectInputStream objectInputStream = null;
        String path = null;
        try {
            System.out.println("Client unity 3d");
            objectInputStream = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            MessageRequest messageRequest = (MessageRequest)objectInputStream.readObject();
            
            String mType = messageRequest.getMessageType();
            MessageResponse messageResponse = null;
            FileInputStream fileInputStream = null;
            FileInputStream fileInputStream3d = null;
            
            if(mType.equals("MediaDataRequest")){
                Data data = new Data();
                byte[] bs = new byte[1024];
                System.out.println("MEDIA DATA REQUEST");
                MediaDataController dataController = new MediaDataController();
//                int validation = dataController.checkValidation((MediaDataRequest)messageRequest, IP);
//                if(validation==1){
                messageResponse = dataController.getData(messageRequest, IP);
                num = ((MediaDataRequest)messageRequest).getNum();
                messageResponse.setMessageType("MediaDataResponse");
                objectOutputStream.writeObject(messageResponse);
                objectOutputStream.flush();
                path = ((MediaDataResponse)messageResponse).getPath();
                System.out.println("FFLUSH Num : "+num);
                final ArrayList<Object> objects = dataController.getPath(messageRequest);
                final File file = new File((String)objects.get(0));

                if(!file.exists()) {
                    bs = "File Not Found ".getBytes();
                    data.setHash(messageResponse.getHash());
                    data.setMessageType("MediaData"+path);
                    data.setBs(bs);
                    objectOutputStream.writeObject((MessageResponse)data);
                    objectOutputStream.writeObject(null);
                    objectOutputStream.flush();
                    objectOutputStream.reset();
                }
                else{
                    
                    long size = file.length();
                    int len;
                    int j=0;

                    hash = messageResponse.getHash();
                    System.out.println("Path : "+path);
                    try {
                        if(path.equals("swf") || path.equals("unity3d")){
                            len = 0;
                            fileInputStream = new FileInputStream(file);
                            System.out.println("File Description : "+size);
                            System.out.println("File Description : "+size/5);
                            fileInputStream.skip(127);
                            fileInputStream.skip(num*(size/5));
                            while(len != size/5){
                                if(num==4){
                                    if(fileInputStream.read(bs)==-1){
                                        break;
                                    }
                                }else{
                                    if(size/5-len <= 1024){
                                        bs = new byte[(int)(size/5-len)];
                                    }
                                    len = len + fileInputStream.read(bs);
                                }
                                data = new Data();
                                data.setHash(messageResponse.getHash());
                                data.setSequenceNumber(size);
                                data.setMessageType("MediaData"+path);
                                data.setBs(bs);
                                objectOutputStream.writeObject((MessageResponse)data);
                                objectOutputStream.flush();
                                objectOutputStream.reset();
                            }
                            System.out.println("len : "+len);
                            data.setHash(messageResponse.getHash());
                            data.setMessageType("MediaData"+path);
                            data.setBs("\nEND".getBytes());
                            objectOutputStream.writeObject((MessageResponse)data);
                            objectOutputStream.flush();
                            objectOutputStream.reset();
                        }else if(path.equals("mp4")){
                            size = (int)objects.get(1);
                            fileInputStream = new FileInputStream(file);
                            System.out.println("File Description : "+fileInputStream.getFD().toString());
                            for(j=0;j<2000; j++){
                                if((len = fileInputStream.read(bs))!=-1){
                                    data = new Data();
                                    data.setHash(messageResponse.getHash());
                                    data.setSequenceNumber(size);
                                    data.setMessageType("MediaData"+path);
                                    data.setBs(bs);
                                    objectOutputStream.writeObject((MessageResponse)data);
                                    objectOutputStream.flush();
                                    objectOutputStream.reset();
                                    bs = new byte[1024];
                                }else
                                    break;
                            }
                        }else if(path.equals("3dmp4")){
                            size = (int)objects.get(1);
                            fileInputStream3d = new FileInputStream(file);
                            System.out.println("File Description : "+fileInputStream.getFD().toString());
                            for(j=0;j<2000; j++){
                                if((len = fileInputStream3d.read(bs))!=-1){
                                    data = new Data();
                                    data.setHash(messageResponse.getHash());
                                    data.setSequenceNumber(size);
                                    data.setMessageType("MediaData"+path);
                                    data.setBs(bs);
                                    objectOutputStream.writeObject((MessageResponse)data);
                                    objectOutputStream.flush();
                                    objectOutputStream.reset();
                                    bs = new byte[1024];
                                }else
                                    break;
                            }
                        }else if(path.equalsIgnoreCase("pdf") || path.equalsIgnoreCase("jpg")){
                            fileInputStream = new FileInputStream(file);
                            System.out.println("File Description : "+fileInputStream.getFD().toString());
                            while(((len = fileInputStream.read(bs))!=-1)){
                                data = new Data();
                                data.setHash(messageResponse.getHash());
                                data.setSequenceNumber(size);
                                data.setMessageType("MediaData"+path);
                                data.setBs(bs);
                                objectOutputStream.writeObject((MessageResponse)data);
                                objectOutputStream.flush();
                                objectOutputStream.reset();
                                bs = new byte[1024];
                            }
                        }
                        System.out.println("DATA");
                        if(j==2000){
                            System.out.println("j==10");
                            data.setHash(messageResponse.getHash());
                            data.setMessageType("MediaData"+path);
                            data.setBs("\ncontinue".getBytes());
                            objectOutputStream.writeObject((MessageResponse)data);
                            objectOutputStream.flush();
                            objectOutputStream.reset();                                                        
                        }else{
                            System.out.println("j!=10");
                            data.setHash(messageResponse.getHash());
                            data.setMessageType("MediaData"+path);
                            data.setBs("\nEND".getBytes());
                            objectOutputStream.writeObject((MessageResponse)data);
                            objectOutputStream.flush();
                            objectOutputStream.reset();
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else if(mType.equals("MediaDataContinueRequest")){
                System.out.println("MediaDataContinueRequest");
                MediaDataContinueRequest continueRequest = (MediaDataContinueRequest) messageRequest;
                Data data = new Data();
                try {
                    int len;
                    byte[] bs = new byte[1024];
                    int j =0;
                    if(continueRequest.getFileType().equals("mp4")){
                        for(j=0;j<2000;j++){
                            if((len = fileInputStream.read(bs))!=-1){
                                data = new Data();
                                data.setHash(hash);
                                data.setMessageType("MediaData"+path);
                                data.setBs(bs);
                                objectOutputStream.writeObject((MessageResponse)data);
                                objectOutputStream.flush();
                                objectOutputStream.reset();
                            }else
                                break;
                        }
                    }else{
                        for(j=0;j<2000;j++){
                            if((len = fileInputStream3d.read(bs))!=-1){
                                data = new Data();
                                data.setHash(hash);
                                data.setMessageType("MediaData"+path);
                                data.setBs(bs);
                                objectOutputStream.writeObject((MessageResponse)data);
                                objectOutputStream.flush();
                                objectOutputStream.reset();
                            }else
                                break;
                        }
                    }
                    if(j==2000){
                        System.out.println("j==10");
                        data.setHash(hash);
                        data.setMessageType("MediaData"+path);
                        data.setBs("\ncontinue".getBytes());
                        objectOutputStream.writeObject((MessageResponse)data);
                        objectOutputStream.flush();
                        objectOutputStream.reset();                                                        
                    }else{
                        System.out.println("j!=10");
                        data.setHash(hash);
                        data.setMessageType("MediaData"+path);
                        data.setBs("\nEND".getBytes());
                        objectOutputStream.writeObject((MessageResponse)data);
                        objectOutputStream.flush();
                        objectOutputStream.reset();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
//            }
        } catch (IOException ex) {
            Logger.getLogger(ClientUnity3d.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClientUnity3d.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                objectInputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(ClientUnity3d.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}