package org.coretechies.Server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.coretechies.Controller.*;
import org.coretechies.MessageTransporter.*;

public class Client implements Runnable{

    Socket socket;
    String IP;
    
    Client(Socket s) {
        this.socket = s;

        InetAddress address = this.socket.getInetAddress();
        this.IP = address.getHostAddress();
        Thread t = new Thread(this);
        System.out.println("client ");
        t.start();
    }

    @Override
    public void run() {
        try {
            int i=1;
            String dataPath = "";
            int validation = 0;
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            
            MessageRequest messageRequest = (MessageRequest)objectInputStream.readObject();
            String mType = messageRequest.getMessageType();
            MessageResponse messageResponse;
            FileInputStream fileInputStream = null;
            FileInputStream fileInputStream3d = null;
            String hash = "";
            CommonController commonController = new CommonController();

            if(mType.equals("Login")){
                
                LoginController loginController = new LoginController();
                System.out.println("LoginRequest");
                validation = loginController.checkValidation((LoginRequest) messageRequest);
                messageResponse = loginController.getResponse(messageRequest, validation, IP);
                objectOutputStream.writeObject(messageResponse);
                objectOutputStream.flush();
            }
            if(validation==1){
                while(true){
                    messageRequest = (MessageRequest)objectInputStream.readObject();
//                    messageRequest = commonController.checkSession(messageRequest);
                    mType = messageRequest.getMessageType();
                    
                    if(mType.equals("Resource")){
                        ResourceController resourceController = new ResourceController();
                        validation = resourceController.checkValidation((ResourceRequest)messageRequest, IP);
                        if(validation==1){
                            messageResponse = resourceController.getResponse(0, IP);
                            objectOutputStream.writeObject(messageResponse);
                            objectOutputStream.flush();
                        }
                    }
                    else{
                        if(mType.equals("Data")){
                            DataController dataController = new DataController();
                            validation = dataController.checkValidation((DataRequest)messageRequest, IP);
                            if(validation==1){
                                dataController.setSingleTonData(messageRequest);
                                messageResponse = dataController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }
                        else if(mType.equals("UnitData")){
                            SubjectController subjectController = new SubjectController();
                            validation = subjectController.checkValidation((SubjectDataRequest)messageRequest, IP);
                            System.out.println("Anand Tiwari");
                            if(validation==1){
                                subjectController.setSingleTonData(messageRequest);
                                messageResponse = subjectController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("ChapterList")){
                            ChapterController chapterController = new ChapterController();
                            validation = chapterController.checkValidation((ChapterRequest)messageRequest, IP);
                            System.out.println("ChapterList");
                            if(validation==1){
                                System.out.println("Inside Chapter request");
                                chapterController.setSingleTonData(messageRequest);
                                messageResponse = chapterController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("LessonList")){
                            LessonController lessonController = new LessonController();
                            validation = lessonController.checkValidation((LessonRequest)messageRequest, IP);
                            System.out.println("LessonList");
                            if(validation==1){
                                lessonController.setSingleTonData(messageRequest);
                                messageResponse = lessonController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("Topic")){
                            AllDataController allDataController = new AllDataController();
                            validation = allDataController.checkValidation((AllDataRequest)messageRequest, IP);
                            System.out.println("Topic");
                            if(validation==1){
                                allDataController.setSingleTonData(messageRequest);
                                messageResponse = allDataController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("CreateUserRequest")){
                            CreateUserController createUserController = new CreateUserController();
                            validation = createUserController.checkValidation(messageRequest, IP);
                            System.out.println("Create User");
                            if(validation==1){
                                messageResponse = createUserController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("StateRequest")){
                            StateController stateController = new StateController();
                            validation = stateController.checkValidation(messageRequest, IP);
                            System.out.println("Create User");
                            if(validation==1){
                                messageResponse = stateController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("SchoolDistrictRequest")){
                            SchoolDistrictController schoolDistrictController = new SchoolDistrictController();
                            validation = schoolDistrictController.checkValidation(messageRequest, IP);
                            System.out.println("Create User");
                            if(validation==1){
                                messageResponse = schoolDistrictController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("SchoolRequest")){
                            SchoolController schoolController = new SchoolController();
                            validation = schoolController.checkValidation((SchoolRequest) messageRequest, IP);
                            System.out.println("Create User9999999999999999999999999"+validation);
                            if(validation==1){
                                System.out.println("Create User99999999");
                                messageResponse = schoolController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("UserNameVerificationRequest")){
                            UserVerificationController userVerificationController = new UserVerificationController();
                            validation = userVerificationController.checkValidation((UserNameVerificationRequest) messageRequest, IP);
                            if(validation==1){
                                System.out.println("Create User99999999");
                                messageResponse = userVerificationController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("ClassDayScheduleRequest")){
                            ClassDayScheduleController classDayScheduleController = new ClassDayScheduleController();
                            validation = classDayScheduleController.checkValidation((ClassDayScheduleRequest) messageRequest, IP);
                            if(validation==1){
                                System.out.println("Create User99999999");
                                messageResponse = classDayScheduleController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("MyLeavesRequest")){
                            MyLeavesController myLeavesController = new MyLeavesController();
                            validation = myLeavesController.checkValidation((MyLeavesRequest) messageRequest, IP);
                            if(validation==1){
                                System.out.println("Create User99999999");
                                messageResponse = myLeavesController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("UpdateLeaveRequest")){
                            UpdateLeaveController updateLeaveController = new UpdateLeaveController();
                            validation = updateLeaveController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = updateLeaveController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("UpdateSchoolHolidayRequest")){
                            UpdateSchoolHolidayController updateSchoolHolidayController = new UpdateSchoolHolidayController();
                            validation = updateSchoolHolidayController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = updateSchoolHolidayController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("SchoolHolidayRequest")){
                            SchoolHolidayController schoolHolidayController = new SchoolHolidayController();
                            validation = schoolHolidayController.checkValidation((SchoolHolidayRequest) messageRequest, IP);
                            if(validation==1){
                                messageResponse = schoolHolidayController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("LessonPlanningRequest")){
                            LessonPlanningController lessonPlanningController = new LessonPlanningController();
                            validation = lessonPlanningController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = lessonPlanningController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("LessonDateRequest")){
                            LessonPlanningDateController lessonPlanningDateController = new LessonPlanningDateController();
                            validation = lessonPlanningDateController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = lessonPlanningDateController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("PastScheduleRequest")){
                            PastLessonScheduleController pastLessonScheduleController = new PastLessonScheduleController();
                            validation = pastLessonScheduleController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = pastLessonScheduleController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("UpcomingScheduleRequest")){
                            UpcomingLessonScheduleController upcomingLessonScheduleController = new UpcomingLessonScheduleController();
                            validation = upcomingLessonScheduleController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = upcomingLessonScheduleController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("ChangePasswordRequest")){
                            ChangePasswordController changePasswordController = new ChangePasswordController();
                            validation = changePasswordController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                System.out.println("Change Password");
                                messageResponse = changePasswordController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("UpdateUserDataRequest")){
                            UpdateUserInfoDataController updateUserInfoDataController = new UpdateUserInfoDataController();
                            validation = updateUserInfoDataController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                System.out.println("Change Password");
                                messageResponse = updateUserInfoDataController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("UpdatePasswordRequest")){
                            UpdatePasswordController updatePasswordController = new UpdatePasswordController();
                            validation = updatePasswordController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                System.out.println("Update Password");
                                messageResponse = updatePasswordController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("CreateSchoolUserSubmitRequest")){
                            CreateUserSubmitController createUserSubmitController = new CreateUserSubmitController();
                            validation = createUserSubmitController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                System.out.println("Change Password");
                                messageResponse = createUserSubmitController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("UpdateSchoolUserSubmitRequest")){
                            UpdateUserSubmitController updateUserSubmitController = new UpdateUserSubmitController();
                            validation = updateUserSubmitController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                System.out.println("Change Password");
                                messageResponse = updateUserSubmitController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("UpdateUserSubmitRequest")){
                            UpdateUserSubmitController updateUserSubmitController = new UpdateUserSubmitController();
                            validation = updateUserSubmitController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                System.out.println("Change Password");
                                messageResponse = updateUserSubmitController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("CreateUserSubmitRequest")){
                            CreateUserSubmitController createUserSubmitController = new CreateUserSubmitController();
                            validation = createUserSubmitController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                System.out.println("Change Password");
                                messageResponse = createUserSubmitController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("AddHolidayrequest")){
                            AddSchoolHolidayController addSchoolHolidayController = new AddSchoolHolidayController();
                            validation = addSchoolHolidayController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = addSchoolHolidayController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("AddLeaveRequest")){
                            AddLeaveController addLeaveController = new AddLeaveController();
                            validation = addLeaveController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = addLeaveController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("DeleteLeaveRequest")){
                            DeleteLeaveController deleteLeaveController = new DeleteLeaveController();
                            validation = deleteLeaveController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = deleteLeaveController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("DeleteLessonPlanRequest")){
                            System.out.println("DeleteLessonPlanRequest");
                            DeleteLessonPlanController deleteLessonPlanController = new DeleteLessonPlanController();
                            validation = deleteLessonPlanController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = deleteLessonPlanController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("DeleteschoolHolidayRequest")){
                            DeleteSchoolHolidayController deleteSchoolHolidayController = new DeleteSchoolHolidayController();
                            validation = deleteSchoolHolidayController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                System.out.println("DETETETE Password");
                                messageResponse = deleteSchoolHolidayController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("UpdateClassDayScheduleRequest")){
                            UpdateClassDayScheduleController updateClassDayScheduleController = new UpdateClassDayScheduleController();
                            validation = updateClassDayScheduleController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = updateClassDayScheduleController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("AddNewLessonSubjectRequest")){
                            AddNewLessonSubjectController addNewLessonSubjectController = new AddNewLessonSubjectController();
                            validation = addNewLessonSubjectController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = addNewLessonSubjectController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("AddNewLessonChapterRequest")){
                            AddNewLessonChapterController addNewLessonChapterController = new AddNewLessonChapterController();
                            validation = addNewLessonChapterController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                System.out.println("Change Password");
                                messageResponse = addNewLessonChapterController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("AddNewLessonLessonRequest")){
                            AddNewLessonLessonController addNewLessonLessonController = new AddNewLessonLessonController();
                            validation = addNewLessonLessonController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = addNewLessonLessonController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("AddNewLessonTopicRequest")){
                            AddNewLessonTopicController addNewLessonTopicController = new AddNewLessonTopicController();
                            validation = addNewLessonTopicController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = addNewLessonTopicController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("SearchDataRequest")){
                            SearchDataController searchDataController = new SearchDataController();
                            validation = searchDataController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = searchDataController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("SearchSortedDataRequest")){
                            SearchSortedDataController searchSortedDataController = new SearchSortedDataController();
                            validation = searchSortedDataController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = searchSortedDataController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("CreateNewLessonRequest")){
                            CreateNewLessonController createNewLessonController = new CreateNewLessonController();
                            validation = createNewLessonController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = createNewLessonController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("LeaveDateRequest")){
                            LeaveDateController leaveDateController = new LeaveDateController();
                            validation = leaveDateController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = leaveDateController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("UpdateUserRequest")){
                            UpdateUserController updateUserController = new UpdateUserController();
                            validation = updateUserController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = updateUserController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("UserNameConfirmationRequest")){
                            UserNameConfirmationController userNameConfirmationController = new UserNameConfirmationController();
                            validation = userNameConfirmationController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = userNameConfirmationController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("LogOutRequest")){
                            LogOutController logOutController = new LogOutController();
                            validation = logOutController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = logOutController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                                socket.close();
                                break;
                            }
                        }else if(mType.equals("SessionLogOutRequest")){
                            LogOutController logOutController = new LogOutController();
                            validation = logOutController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = logOutController.getSessionData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                                socket.close();
                                break;
                            }
//                            socket.close();
                        }else if(mType.equals("UpdateLessonPlanRequest")){
                            UpdateLessonPlanController updateLessonPlanController = new UpdateLessonPlanController();
                            validation = updateLessonPlanController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                messageResponse = updateLessonPlanController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("MonthlyScheduleRequest")){
                            System.out.println("Monthly Schedule Request");
                            MonthlyScheduleController monthlyScheduleController = new MonthlyScheduleController();
                            validation = monthlyScheduleController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                System.out.println("Update User");
                                messageResponse = monthlyScheduleController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("WeeklyScheduleRequest")){
                            System.out.println("Monthly Schedule Request");
                            WeeklyScheduleController weeklyScheduleController = new WeeklyScheduleController();
                            validation = weeklyScheduleController.checkValidation(messageRequest, IP);
                            if(validation==1){
                                System.out.println("Update User");
                                messageResponse = weeklyScheduleController.getData(messageRequest, IP);
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                            }
                        }else if(mType.equals("MediaDataRequest")){
                            System.out.println("MEDIA DATA REQUEST");
                            MediaDataController dataController = new MediaDataController();
                            validation = dataController.checkValidation((MediaDataRequest)messageRequest, IP);
                            if(validation==1){
                                messageResponse = dataController.getData(messageRequest, IP);
                                messageResponse.setMessageType("MediaDataResponse");
                                objectOutputStream.writeObject(messageResponse);
                                objectOutputStream.flush();
                                String path = ((MediaDataResponse)messageResponse).getPath();
                                System.out.println("FFLUSH");
                                Data data = new Data();
                                final ArrayList<Object> objects = dataController.getPath(messageRequest);
                                final File file = new File((String)objects.get(0));
                                byte[] bs = new byte[127];
                                if(!file.exists()) {
                                    bs = "File Not Found ".getBytes();
                                    data.setHash(messageResponse.getHash());
                                    data.setMessageType("MediaData"+path);
                                    data.setBs(bs);
                                    objectOutputStream.writeObject((MessageResponse)data);
                                    objectOutputStream.writeObject(null);
                                    objectOutputStream.flush();
                                    objectOutputStream.reset();
                                }
                                else{
                                    dataPath = path;
                                    if(path.equalsIgnoreCase("swf")){
                                        
                                    }else if(path.equalsIgnoreCase("unity3d")){
//                                        unity3dfile = file;
                                    }else{
                                        try {
                                            long size = file.length();
                                            int len;
                                            int j=0;
                                            hash = messageResponse.getHash();
                                            System.out.println("Path : "+path);
                                            if(path.equals("mp4")){
                                                size = (int)objects.get(1);
                                                fileInputStream = new FileInputStream(file);
                                                System.out.println("File Description : "+fileInputStream.getFD().toString());
                                                for(j=0;j<2000; j++){
                                                    if((len = fileInputStream.read(bs))!=-1){
                                                        data = new Data();
                                                        data.setHash(messageResponse.getHash());
                                                        data.setSequenceNumber(size);
                                                        data.setMessageType("MediaData"+dataPath);
                                                        data.setBs(bs);
                                                        objectOutputStream.writeObject((MessageResponse)data);
                                                        objectOutputStream.flush();
                                                        objectOutputStream.reset();
                                                        bs = new byte[1024];
                                                    }else
                                                        break;
                                                }
                                            }else if(path.equals("3dmp4")){
                                                size = (int)objects.get(1);
                                                fileInputStream3d = new FileInputStream(file);
                                                System.out.println("File Description : "+fileInputStream.getFD().toString());
                                                for(j=0;j<2000; j++){
                                                    if((len = fileInputStream3d.read(bs))!=-1){
                                                        data = new Data();
                                                        data.setHash(messageResponse.getHash());
                                                        data.setSequenceNumber(size);
                                                        data.setMessageType("MediaData"+dataPath);
                                                        data.setBs(bs);
                                                        objectOutputStream.writeObject((MessageResponse)data);
                                                        objectOutputStream.flush();
                                                        objectOutputStream.reset();
                                                        bs = new byte[1024];
                                                    }else
                                                        break;
                                                }
                                            }else if(path.equalsIgnoreCase("pdf") || path.equalsIgnoreCase("jpg")){
                                                fileInputStream = new FileInputStream(file);
                                                System.out.println("File Description : "+fileInputStream.getFD().toString());
                                                while(((len = fileInputStream.read(bs))!=-1)){
                                                    data = new Data();
                                                    data.setHash(messageResponse.getHash());
                                                    data.setSequenceNumber(size);
                                                    data.setMessageType("MediaData"+dataPath);
                                                    data.setBs(bs);
                                                    objectOutputStream.writeObject((MessageResponse)data);
                                                    objectOutputStream.flush();
                                                    objectOutputStream.reset();
                                                    bs = new byte[1024];
                                                }
                                            }
                                            System.out.println("DATA");
                                            if(j==2000){
                                                System.out.println("j==10");
                                                data.setHash(messageResponse.getHash());
                                                data.setMessageType("MediaData"+dataPath);
                                                data.setBs("\ncontinue".getBytes());
                                                objectOutputStream.writeObject((MessageResponse)data);
                                                objectOutputStream.flush();
                                                objectOutputStream.reset();                                                        
                                            }else{
                                                System.out.println("j!=10");
                                                data.setHash(messageResponse.getHash());
                                                data.setMessageType("MediaData"+dataPath);
                                                data.setBs("\nEND".getBytes());
                                                objectOutputStream.writeObject((MessageResponse)data);
                                                objectOutputStream.flush();
                                                objectOutputStream.reset();
                                            }
                                        } catch (IOException ex) {
                                            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                }
                            }
                        }else if(mType.equals("MediaDataContinueRequest")){
                            System.out.println("MediaDataContinueRequest");
                            MediaDataContinueRequest continueRequest = (MediaDataContinueRequest) messageRequest;
                            Data data = new Data();
                            try {
                                int len;
                                byte[] bs = new byte[1024];
                                int j =0;
                                if(continueRequest.getFileType().equals("mp4")){
                                    for(j=0;j<2000;j++){
                                        if((len = fileInputStream.read(bs))!=-1){
                                            data = new Data();
                                            data.setHash(hash);
                                            data.setMessageType("MediaData"+dataPath);
                                            data.setBs(bs);
                                            objectOutputStream.writeObject((MessageResponse)data);
                                            objectOutputStream.flush();
                                            objectOutputStream.reset();
                                        }else
                                            break;
                                    }
                                }else{
                                    for(j=0;j<2000;j++){
                                        if((len = fileInputStream3d.read(bs))!=-1){
                                            data = new Data();
                                            data.setHash(hash);
                                            data.setMessageType("MediaData"+dataPath);
                                            data.setBs(bs);
                                            objectOutputStream.writeObject((MessageResponse)data);
                                            objectOutputStream.flush();
                                            objectOutputStream.reset();
                                        }else
                                            break;
                                    }
                                }
                                if(j==2000){
                                    System.out.println("j==10");
                                    data.setHash(hash);
                                    data.setMessageType("MediaData"+dataPath);
                                    data.setBs("\ncontinue".getBytes());
                                    objectOutputStream.writeObject((MessageResponse)data);
                                    objectOutputStream.flush();
                                    objectOutputStream.reset();                                                        
                                }else{
                                    System.out.println("j!=10");
                                    data.setHash(hash);
                                    data.setMessageType("MediaData"+dataPath);
                                    data.setBs("\nEND".getBytes());
                                    objectOutputStream.writeObject((MessageResponse)data);
                                    objectOutputStream.flush();
                                    objectOutputStream.reset();
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
            else{
                String path = "";
                byte[] bs = new byte[1024];
//                Data data = new Data();
                int num=0;
                do{
                    if(mType.equals("MediaDataRequest")){
                        System.out.println("MEDIA DATA REQUEST");
                        MediaDataController dataController = new MediaDataController();
                        messageResponse = dataController.getData(messageRequest, IP);
                        num = ((MediaDataRequest)messageRequest).getNum();
                        messageResponse.setMessageType("MediaDataResponse");
                        objectOutputStream.writeObject(messageResponse);
                        objectOutputStream.flush();
                        path = ((MediaDataResponse)messageResponse).getPath();
                        System.out.println("FFLUSH Num : "+num);
                        System.out.println("FFLUSH Num : "+path);
                        final ArrayList<Object> objects = dataController.getPath(messageRequest);
                        final File file = new File((String)objects.get(0));

                        if(!file.exists()) {
                            bs = "File Not Found ".getBytes();
                            Data data = new Data();
                            data.setHash(messageResponse.getHash());
                            data.setMessageType("MediaData"+path);
                            data.setBs(bs);
                            objectOutputStream.writeObject((MessageResponse)data);
                            objectOutputStream.writeObject(null);
                            objectOutputStream.flush();
                            objectOutputStream.reset();
                        } else{
                            long size = file.length();
                            int len;
                            int j=0;
                            try {
                                System.out.println("size : "+size);
                                if(path.equals("swf") || path.equals("unity3d")){
                                    len = 0;
                                    fileInputStream = new FileInputStream(file);
                                    System.out.println("File Description : "+size);
                                    System.out.println("File Description : "+size/10);
                                    fileInputStream.skip(127);
                                    fileInputStream.skip(num*(size/10));
                                    while(len != size/10){
                                        if(num==9){
                                            if(fileInputStream.read(bs)==-1){
                                                break;
                                            }
                                        }else{
                                            if(size/10-len <= 1024){
                                                bs = new byte[(int)(size/10-len)];
                                            }
                                            len = len + fileInputStream.read(bs);
                                        }
                                        Data data = new Data();
                                        data.setHash(messageResponse.getHash());
                                        data.setSequenceNumber(size);
                                        data.setMessageType("MediaData"+path);
                                        data.setBs(bs);
                                        objectOutputStream.writeObject((MessageResponse)data);
                                        objectOutputStream.flush();
                                        objectOutputStream.reset();
                                    }
                                    System.out.println("len : "+len);
                                    Data data = new Data();
                                    data.setHash(messageResponse.getHash());
                                    data.setMessageType("MediaData"+path);
                                    data.setBs("\nEND".getBytes());
                                    objectOutputStream.writeObject((MessageResponse)data);
                                    objectOutputStream.flush();
                                    objectOutputStream.reset();
                                }else{
                                    hash = messageResponse.getHash();
                                    System.out.println("Path : "+path);
                                    if(path.equals("mp4")){
                                        bs = new byte[127];
                                        size = (int)objects.get(1);
                                        fileInputStream = new FileInputStream(file);
                                        System.out.println("File Description : "+fileInputStream.getFD().toString());
                                        for(j=0;j<2000; j++){
                                            Data data = new Data();
                                            if((len = fileInputStream.read(bs))!=-1){
                                                System.out.println("j : "+j);
                                                data.setHash(messageResponse.getHash());
                                                data.setSequenceNumber(size);
                                                data.setMessageType("MediaData"+path);
                                                System.out.println("message Type : "+data.getMessageType());
                                                data.setBs(bs);
                                                objectOutputStream.writeObject((MessageResponse)data);
                                                objectOutputStream.flush();
                                                objectOutputStream.reset();
                                                bs = new byte[1024];
                                            }else
                                                break;
                                        }
                                    }else if(path.equals("3dmp4")){
                                        size = (int)objects.get(1);
                                        bs = new byte[127];
                                        fileInputStream3d = new FileInputStream(file);
                                        System.out.println("File Description : "+fileInputStream3d.getFD().toString());
                                        for(j=0;j<2000; j++){
                                            System.out.println("j="+j);
                                            if((len = fileInputStream3d.read(bs))!=-1){
                                                Data data = new Data();
                                                data.setHash(messageResponse.getHash());
                                                data.setSequenceNumber(size);
                                                data.setMessageType("MediaData"+path);
                                                data.setBs(bs);
                                                objectOutputStream.writeObject((MessageResponse)data);
                                                objectOutputStream.flush();
                                                objectOutputStream.reset();
                                                bs = new byte[1024];
                                            }else
                                                break;
                                        }
                                    }else if(path.equalsIgnoreCase("pdf") || path.equalsIgnoreCase("jpg")){
                                        fileInputStream = new FileInputStream(file);
                                        System.out.println("File Description : "+fileInputStream.getFD().toString());
                                        while(((len = fileInputStream.read(bs))!=-1)){
                                            Data data = new Data();
                                            data.setHash(messageResponse.getHash());
                                            data.setSequenceNumber(size);
                                            data.setMessageType("MediaData"+path);
                                            data.setBs(bs);
                                            objectOutputStream.writeObject((MessageResponse)data);
                                            objectOutputStream.flush();
                                            objectOutputStream.reset();
                                            bs = new byte[1024];
                                        }
                                    }
                                    System.out.println("DATA");
                                    if(j==2000){
                                        System.out.println("j==10");
                                        Data data = new Data();
                                        data.setHash(messageResponse.getHash());
                                        data.setMessageType("MediaData"+path);
                                        data.setBs("\ncontinue".getBytes());
                                        objectOutputStream.writeObject((MessageResponse)data);
                                        objectOutputStream.flush();
                                        objectOutputStream.reset();                                                        
                                    }else{
                                        Data data = new Data();
                                        System.out.println("j!=10");
                                        data.setHash(messageResponse.getHash());
                                        data.setMessageType("MediaData"+path);
                                        data.setBs("\nEND".getBytes());
                                        objectOutputStream.writeObject((MessageResponse)data);
                                        objectOutputStream.flush();
                                        objectOutputStream.reset();
                                    }
                                }
                            } catch (IOException ex) {
                                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }else if(mType.equals("MediaDataContinueRequest")){
                        System.out.println("MediaDataContinueRequest");
                        MediaDataContinueRequest continueRequest = (MediaDataContinueRequest) messageRequest;
                        Data data = new Data();
                        try {
                            int len;
                            bs = new byte[1024];
                            int j =0;
                            if(continueRequest.getFileType().equals("mp4")){
                                for(j=0;j<2000;j++){
                                    if((len = fileInputStream.read(bs))!=-1){
                                        data = new Data();
                                        data.setHash(hash);
                                        data.setMessageType("MediaData"+path);
                                        data.setBs(bs);
                                        objectOutputStream.writeObject((MessageResponse)data);
                                        objectOutputStream.flush();
                                        objectOutputStream.reset();
                                    }else
                                        break;
                                }
                            }else{
                                for(j=0;j<2000;j++){
                                    if((len = fileInputStream3d.read(bs))!=-1){
                                        data = new Data();
                                        data.setHash(hash);
                                        data.setMessageType("MediaData"+path);
                                        data.setBs(bs);
                                        objectOutputStream.writeObject((MessageResponse)data);
                                        objectOutputStream.flush();
                                        objectOutputStream.reset();
                                    }else
                                        break;
                                }
                            }
                            if(j==2000){
                                System.out.println("j==10");
                                data.setHash(hash);
                                data.setMessageType("MediaData"+path);
                                data.setBs("\ncontinue".getBytes());
                                objectOutputStream.writeObject((MessageResponse)data);
                                objectOutputStream.flush();
                                objectOutputStream.reset();                                                        
                            }else{
                                System.out.println("j!=10");
                                data.setHash(hash);
                                data.setMessageType("MediaData"+path);
                                data.setBs("\nEND".getBytes());
                                objectOutputStream.writeObject((MessageResponse)data);
                                objectOutputStream.flush();
                                objectOutputStream.reset();
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    messageRequest = (MessageRequest)objectInputStream.readObject();
                    mType = messageRequest.getMessageType();
                }while(true);
            }
        } catch (IOException e) {
            try {
                String string = "DELETE FROM `session` WHERE IPAddress = \""+IP+"\"";
                int i = DBConnect.insertData(string);
                socket.close();
            } catch (IOException ei) {
            
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            System.out.println("socket");
            socket.close();
        } catch (IOException e) {
            
        }
    }
}