/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.coretechies.Server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tiwari
 */
public class DBConnect {
    public static Connection getConnection() throws ClassNotFoundException, SQLException
    {
      Connection conn = null;
      Class.forName("com.mysql.jdbc.Driver");
      conn = DriverManager.getConnection("jdbc:mysql://localhost/cdc_beta","root","admin");
      return conn;
    } 
    
    public static int insertData(String query){
        Connection conn = null;
        try {
            conn = getConnection();
            Statement stmt = conn.createStatement();
            return stmt.executeUpdate(query);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
}
